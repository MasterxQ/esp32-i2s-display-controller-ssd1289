#include <display_driver.h>
#include <display_interface.h>

#include "stdio.h"
#include "string.h"
#include "freertos/FreeRTOS.h"
#include "esp_system.h"
// #include "include/lcd_ssd1289.h"
#include <freertos/task.h>
#define LOG_LOCAL_LEVEL ESP_LOG_VERBOSE
#include <esp_log.h>
#include "sdkconfig.h"
#include <driver/gpio.h>

static const char TAG[] = "ILI9341";

volatile display_mem_orga_e cur_mem_orga = DISPLAY_MEM_ORGA_0;


#ifdef CONFIG_DLH_LCD_INTERFACE_HAS_4WIRE_SPI_BUS
#include <driver/spi_master.h>
#define CONDITIONAL_SWAP16(x) SPI_SWAP_DATA_TX(x, 16)
#else
#define CONDITIONAL_SWAP16(x) x
#endif

#define DEFAULT_MEMMORY_ACCESS_CONTROL_REG 0x08

void dpl_set_box(uint16_t x, uint16_t y, uint16_t x_size, uint16_t y_size)
{
	
	uint16_t x_end = x + (x_size - 1);
	uint16_t y_end = y + (y_size - 1);
	dpl_interface_cmd8(0x2A); //column
	dpl_interface_data16(CONDITIONAL_SWAP16(x));
	dpl_interface_data16(CONDITIONAL_SWAP16(x_end));
	dpl_interface_cmd8(0x2B); //page
	dpl_interface_data16(CONDITIONAL_SWAP16(y));
	dpl_interface_data16(CONDITIONAL_SWAP16(y_end));
	dpl_interface_cmd8(0x2C); //write
}

void dpl_draw_section(void* src, uint16_t x, uint16_t y, uint16_t width, uint16_t height)
{
// 	ESP_LOGI("DRAW", "setting area (%d,%d,%d,%d)",x,y,width,height);
	dpl_set_box(x, y, width, height);
// 	i2s_dpl_interface_data8(src, width * height * 2, 100/portTICK_RATE_MS, false);
	for(uint32_t i = 0; i < width * height; i++)
	{
		dpl_interface_data16(*(((uint16_t *)src) +i));
	}
}

void dpl_fill_screen(uint16_t color) {
	dpl_set_box(0,0,240,320);
	for(uint32_t i = 0; i < 240 * 320; i++)
	{
		dpl_interface_data16(color);
	}
}


void dpl_set_rotation(display_mem_orga_e mem_orga, bool force)
{
	if(mem_orga == cur_mem_orga && !force)
	{
		return;
	}
	
	ESP_LOGI(TAG, "Changing rotation: %d, forced: %d", mem_orga, force);
	dpl_interface_cmd8(0x36);
	switch(mem_orga)
	{
		case DISPLAY_MEM_ORGA_0:
			dpl_interface_data8(0b00000000 | DEFAULT_MEMMORY_ACCESS_CONTROL_REG);
			break;
		case DISPLAY_MEM_ORGA_90:
			dpl_interface_data8(0b01100000 | DEFAULT_MEMMORY_ACCESS_CONTROL_REG); //MY,MX,MV,ML,BGR,MH,0,0
			break;
		case DISPLAY_MEM_ORGA_180:
			dpl_interface_data8(0b11000000 | DEFAULT_MEMMORY_ACCESS_CONTROL_REG); //MY,MX,MV,ML,BGR,MH,0,0
			break;
		case DISPLAY_MEM_ORGA_270:
			dpl_interface_data8(0b10100000 | DEFAULT_MEMMORY_ACCESS_CONTROL_REG); //MY,MX,MV,ML,BGR,MH,0,0
			break;
	}
	cur_mem_orga = mem_orga;
}

void dpl_fill_area(uint16_t color) {
	dpl_set_box(0,0,240,320);
	for(uint32_t i = 0; i < 240 * 320; i++)
	{
		dpl_interface_data16(color);
	}
}

bool dpl_chipset_init(void)
{
#ifdef CONFIG_DLH_LCD_USE_RESET
	gpio_set_level(CONFIG_DLH_LCD_PIN_RESET, false);
	vTaskDelay(50 / portTICK_RATE_MS);
	gpio_set_level(CONFIG_DLH_LCD_PIN_RESET, true);
#endif
// 	iot_i2s_lcd_write_cmd(i2s_lcd_handle, 0x01);
	ESP_LOGD(TAG, "Starting display init in 10ms\n");
	vTaskDelay(10 / portTICK_RATE_MS);

	dpl_interface_cmd8(0x01);
	vTaskDelay(30 / portTICK_RATE_MS);
	dpl_interface_cmd8(0xcf); 
	dpl_interface_data8(0x00);
	dpl_interface_data8(0xc1);
	dpl_interface_data8(0x30);

	dpl_interface_cmd8(0xed); 
	dpl_interface_data8(0x64);
	dpl_interface_data8(0x03);
	dpl_interface_data8(0x12);
	dpl_interface_data8(0x81);

	dpl_interface_cmd8(0xcb); 
	dpl_interface_data8(0x39);
	dpl_interface_data8(0x2c);
	dpl_interface_data8(0x00);
	dpl_interface_data8(0x34);
	dpl_interface_data8(0x02);

	dpl_interface_cmd8(0xea); 
	dpl_interface_data8(0x00);
	dpl_interface_data8(0x00);

	dpl_interface_cmd8(0xe8); 
	dpl_interface_data8(0x85);
	dpl_interface_data8(0x10);
	dpl_interface_data8(0x79);

	dpl_interface_cmd8(0xC0); //Power control
	dpl_interface_data8(0x23); //VRH[5:0]

	dpl_interface_cmd8(0xC1); //Power control
	dpl_interface_data8(0x11); //SAP[2:0];BT[3:0]

	dpl_interface_cmd8(0xC2);
	dpl_interface_data8(0x11);

	dpl_interface_cmd8(0xC5); //VCM control
	dpl_interface_data8(0x3d);
	dpl_interface_data8(0x30);

	dpl_interface_cmd8(0xc7); 
	dpl_interface_data8(0xaa);

	dpl_interface_cmd8(0x3A); 
	dpl_interface_data8(0x55);

	dpl_interface_cmd8(0x36); // Memory Access Control
	dpl_interface_data8(DEFAULT_MEMMORY_ACCESS_CONTROL_REG);

	dpl_interface_cmd8(0xB1); // Frame Rate Control
	dpl_interface_data8(0x00);
	dpl_interface_data8(0x11);

	dpl_interface_cmd8(0xB6); // Display Function Control
	dpl_interface_data8(0x0a);
	dpl_interface_data8(0xa2);

	dpl_interface_cmd8(0xF2); // 3Gamma Function Disable
	dpl_interface_data8(0x00);

	dpl_interface_cmd8(0xF7);
	dpl_interface_data8(0x20);

	dpl_interface_cmd8(0xF1);
	dpl_interface_data8(0x01);
	dpl_interface_data8(0x30);

	dpl_interface_cmd8(0x26); //Gamma curve selected
	dpl_interface_data8(0x01);

	dpl_interface_cmd8(0xE0); //Set Gamma
	dpl_interface_data8(0x0f);
	dpl_interface_data8(0x3f);
	dpl_interface_data8(0x2f);
	dpl_interface_data8(0x0c);
	dpl_interface_data8(0x10);
	dpl_interface_data8(0x0a);
	dpl_interface_data8(0x53);
	dpl_interface_data8(0xd5);
	dpl_interface_data8(0x40);
	dpl_interface_data8(0x0a);
	dpl_interface_data8(0x13);
	dpl_interface_data8(0x03);
	dpl_interface_data8(0x08);
	dpl_interface_data8(0x03);
	dpl_interface_data8(0x00);

	dpl_interface_cmd8(0xE1); //Set Gamma
	dpl_interface_data8(0x00);
	dpl_interface_data8(0x00);
	dpl_interface_data8(0x10);
	dpl_interface_data8(0x03);
	dpl_interface_data8(0x0f);
	dpl_interface_data8(0x05);
	dpl_interface_data8(0x2c);
	dpl_interface_data8(0xa2);
	dpl_interface_data8(0x3f);
	dpl_interface_data8(0x05);
	dpl_interface_data8(0x0e);
	dpl_interface_data8(0x0c);
	dpl_interface_data8(0x37);
	dpl_interface_data8(0x3c);
	dpl_interface_data8(0x0F);
	dpl_interface_cmd8(0x11); //Exit Sleep
	vTaskDelay(10 / portTICK_RATE_MS);
	dpl_interface_cmd8(0x29); //display on
	dpl_interface_cmd8(0x2c); //display on
// 	vTaskDelay(20 / portTICK_RATE_MS);
	ESP_LOGD(TAG, "Init done");
	return true;
}

bool dpl_chipset_deinit(void)
{
	dpl_interface_cmd8(0x28); //Display Off
	dpl_interface_cmd8(0x10); //Enter Sleep
	vTaskDelay(120/portTICK_PERIOD_MS); // Datasheet says to wait 120msec until cutting displays power
	ESP_LOGD(TAG, "Deinit done");
	return true;
}
