
#include <lcd_i2s.h>

#include "stdio.h"
#include "string.h"
#include "freertos/FreeRTOS.h"
#include "esp_system.h"
#include <lcd_i2s_com.h>
// #include "include/lcd_ssd1289.h"
#include "sdkconfig.h"

// lcd_ssd1289_dev_t ssd1289_device = 
// {
// 	.xset_cmd = 0,
// 	.yset_cmd = 0,
// 	.lcd_buf = NULL,
// };

// void lcd_ssd1289_set_orientation(lcd_ssd1289_handle_t dp_handle)
// {
//     uint16_t swap = 0;
//     lcd_ssd1289_dev_t *device = (lcd_ssd1289_dev_t *)dp_handle;
//     i2s_lcd_handle_t i2s_lcd_handle = device->i2s_lcd_handle;
//     switch (orientation) {
//     case LCD_DISP_ROTATE_0:
//         iot_i2s_lcd_write_reg(i2s_lcd_handle, ILI9806_MADCTL, 0x00 | 0x00);
//         device->xset_cmd = ILI9806_CASET;
//         device->yset_cmd = ILI9806_RASET;
//         break;
//     case LCD_DISP_ROTATE_90:
//         iot_i2s_lcd_write_reg(i2s_lcd_handle, ILI9806_MADCTL, 0xA0 | 0x00);
//         swap = device->x_size;
//         device->x_size = device->y_size;
//         device->y_size = swap;
//         device->xset_cmd = ILI9806_RASET;
//         device->yset_cmd = ILI9806_CASET;
//         break;
//     case LCD_DISP_ROTATE_180:
//         iot_i2s_lcd_write_reg(i2s_lcd_handle, ILI9806_MADCTL, 0xC0 | 0x00);
//         device->xset_cmd = ILI9806_CASET;
//         device->yset_cmd = ILI9806_RASET;
//         break;
//     case LCD_DISP_ROTATE_270:
//         iot_i2s_lcd_write_reg(i2s_lcd_handle, ILI9806_MADCTL, 0x60 | 0x00);
//         swap = device->x_size;
//         device->x_size = device->y_size;
//         device->y_size = swap;
//         device->xset_cmd = ILI9806_RASET;
//         device->yset_cmd = ILI9806_CASET;
//         break;
//     default:
//         iot_i2s_lcd_write_reg(i2s_lcd_handle, ILI9806_MADCTL, 0x00 | 0x00);
//         device->xset_cmd = ILI9806_CASET;
//         device->yset_cmd = ILI9806_RASET;
//         break;
//     }
// }

void lcd_set_box(uint16_t x, uint16_t y, uint16_t x_size, uint16_t y_size)
{
    uint16_t x_end = x + (x_size - 1);
    uint16_t y_end = y + (y_size - 1);
	
		iot_i2s_lcd_write_reg(0x44, (x_end << 8) | x);

		iot_i2s_lcd_write_reg(0x45,y);

		iot_i2s_lcd_write_reg(0x46, y_end);

		iot_i2s_lcd_write_reg(0x4E,x);

		iot_i2s_lcd_write_reg(0x4F,y);

		iot_i2s_lcd_write_cmd(0x0022);

//     lcd_write_com_data(0x0044, (x2 << 8) + x1);
//     lcd_write_com_data(0x0045, y1);
//     lcd_write_com_data(0x0046, y2);
//     lcd_write_com_data(0x004E, x1);
//     lcd_write_com_data(0x004F, y1);
//     lcd_write_com(0x0022);
}

void lcd_draw_section ( void* src, uint16_t x, uint16_t y, uint16_t width, uint16_t height )
{
// 	ESP_LOGI("DRAW", "setting area (%d,%d,%d,%d)",x,y,width,height);
	lcd_set_box(x, y, width, height);
// 	i2s_lcd_write_data(src, width * height * 2, 100/portTICK_RATE_MS, false);
	for(uint32_t i = 0; i < width * height; i++)
	{
		iot_i2s_lcd_write_data(*(((uint16_t *)src) +i));
	}
}


void lcd_refresh(void)
{
//     iot_i2s_lcd_write(ssd1289_device.lcd_buf, CONFIG_DLH_LCD_DISPLAY_SIZE_X * CONFIG_DLH_LCD_DISPLAY_SIZE_Y * LCD_SSD1289_PIXEL_SIZE);
}

// void lcd_fill_screen(uint16_t color)
// {
//     uint16_t *p = ssd1289_device.lcd_buf;
//     lcd_set_box(0, 0, CONFIG_DLH_LCD_DISPLAY_SIZE_X, CONFIG_DLH_LCD_DISPLAY_SIZE_Y);
//     for (int i = 0; i < CONFIG_DLH_LCD_DISPLAY_SIZE_X * CONFIG_DLH_LCD_DISPLAY_SIZE_Y; i++) {
//         p[i] = color;
//     }
//     iot_i2s_lcd_write(ssd1289_device.lcd_buf, CONFIG_DLH_LCD_DISPLAY_SIZE_X * CONFIG_DLH_LCD_DISPLAY_SIZE_Y * LCD_SSD1289_PIXEL_SIZE);
// }

void lcd_draw_bmp(uint16_t *bmp, uint16_t x, uint16_t y, uint16_t x_size, uint16_t y_size)
{
	lcd_set_box(x, y, x_size, y_size);
	iot_i2s_lcd_write(bmp, x_size * y_size * LCD_SSD1289_PIXEL_SIZE);
}

// void lcd_put_char(uint8_t *str, uint16_t x, uint16_t y, uint16_t x_size, uint16_t y_size, uint16_t wcolor, uint16_t bcolor)
// {
//     uint16_t *pbuf;
//     uint8_t *pdata = str;
//     for (int i = 0; i < y_size; i++) {
//         pbuf = ssd1289_device.lcd_buf + (x + (i + y) * CONFIG_DLH_LCD_DISPLAY_SIZE_X);
//         for (int j = 0; j < x_size / 8; j++) {
//             for (int k = 0; k < 8; k++) {
//                 if (*pdata & (0x80 >> k)) {
//                     *pbuf = wcolor;
//                 } else {
//                     *pbuf = bcolor;
//                 }
//                 pbuf++;
//             }
//             pdata++;
//         }
//     }
//     lcd_refresh();
// }





bool lcd_init(void)
{
// 	if(!ssd1289_device.lcd_buf)
// 	{
// 		uint16_t *p = malloc(sizeof(uint16_t) * CONFIG_DLH_LCD_DISPLAY_SIZE_X * CONFIG_DLH_LCD_DISPLAY_SIZE_Y);
// 		if (p == NULL) {
// 			LCD_LOG("malloc fail\n");
// 			goto error;
// 		}
// 		ssd1289_device.lcd_buf = p;
// 	}

// 	i2s_lcd_config_t pin_conf = 
// 	{
// 		.data_width = 16,          /*!< Parallel data width, 16bit or 8bit available */
// 		.data_io_num = {
// 			CONFIG_DLH_LCD_PIN_DB0,
// 			CONFIG_DLH_LCD_PIN_DB1,
// 			CONFIG_DLH_LCD_PIN_DB2,
// 			CONFIG_DLH_LCD_PIN_DB3,
// 			CONFIG_DLH_LCD_PIN_DB4,
// 			CONFIG_DLH_LCD_PIN_DB5,
// 			CONFIG_DLH_LCD_PIN_DB6,
// 			CONFIG_DLH_LCD_PIN_DB7,
// 			CONFIG_DLH_LCD_PIN_DB8,
// 			CONFIG_DLH_LCD_PIN_DB9,
// 			CONFIG_DLH_LCD_PIN_DB10,
// 			CONFIG_DLH_LCD_PIN_DB11,
// 			CONFIG_DLH_LCD_PIN_DB12,
// 			CONFIG_DLH_LCD_PIN_DB13,
// 			CONFIG_DLH_LCD_PIN_DB14,
// 			CONFIG_DLH_LCD_PIN_DB15
// 		},     /*!< Parallel data output IO*/
// 		.ws_io_num = CONFIG_DLH_LCD_PIN_WR,           /*!< write clk io*/
// 		.rs_io_num = CONFIG_DLH_LCD_PIN_RS,           /*!< rs io num */
// #ifdef CONFIG_DLH_LCD_USE_RESET
// 		.reset_io_num = CONFIG_DLH_LCD_PIN_RESET,
// #endif
// 	};
	
	if(!iot_i2s_lcd_pin_cfg())
	{
		LCD_LOG("Failed to configure LCD pins");
		return false;
	}	

	
#ifdef CONFIG_DLH_LCD_USE_RESET
	gpio_set_level(CONFIG_DLH_LCD_PIN_RESET, false);
	vTaskDelay(50 / portTICK_RATE_MS);
	gpio_set_level(CONFIG_DLH_LCD_PIN_RESET, true);
#endif
// 	iot_i2s_lcd_write_cmd(i2s_lcd_handle, 0x01);
	printf("Starting display init in 10ms\n");
	vTaskDelay(50 / portTICK_RATE_MS);

	iot_i2s_lcd_write_reg(0x0000,0x0001);
	printf("first command done\n");
	vTaskDelay(100 / portTICK_RATE_MS);
	iot_i2s_lcd_write_reg(0x0003,0xA8A4);
	iot_i2s_lcd_write_reg(0x000C,0x0000);
	iot_i2s_lcd_write_reg(0x000D,0x080C);
	iot_i2s_lcd_write_reg(0x000E,0x2B00);
	iot_i2s_lcd_write_reg(0x001E,0x00B0);
							  //  0RRCBSTMMMMMMMMM  // Change C to swap odd and even
	iot_i2s_lcd_write_reg(0x0001,0b0011101100111111);    vTaskDelay(20 / portTICK_RATE_MS);   /* 320*240 0x2B3F */
	iot_i2s_lcd_write_reg(0x0002,0x0600);
	iot_i2s_lcd_write_reg(0x0010,0x0000);
	iot_i2s_lcd_write_reg(0x0011,0x6070);
	iot_i2s_lcd_write_reg(0x0005,0x0000);
	iot_i2s_lcd_write_reg(0x0006,0x0000);
	iot_i2s_lcd_write_reg(0x0016,0xEF1C);
	iot_i2s_lcd_write_reg(0x0017,0x0003);
	iot_i2s_lcd_write_reg(0x0007,0x0333);      
	iot_i2s_lcd_write_reg(0x000B,0x0000);
	iot_i2s_lcd_write_reg(0x000F,0x0000);
	iot_i2s_lcd_write_reg(0x0041,0x0000);
	iot_i2s_lcd_write_reg(0x0042,0x0000);
	iot_i2s_lcd_write_reg(0x0048,0x0000);
	iot_i2s_lcd_write_reg(0x0049,0x013F);
	iot_i2s_lcd_write_reg(0x004A,0x0000);
	iot_i2s_lcd_write_reg(0x004B,0x0000);
	iot_i2s_lcd_write_reg(0x0044,0xEF00);
	iot_i2s_lcd_write_reg(0x0045,0x0000);
	iot_i2s_lcd_write_reg(0x0046,0x013F);
	iot_i2s_lcd_write_reg(0x0030,0x0707);
	iot_i2s_lcd_write_reg(0x0031,0x0204);
	iot_i2s_lcd_write_reg(0x0032,0x0204);
	iot_i2s_lcd_write_reg(0x0033,0x0502);
	iot_i2s_lcd_write_reg(0x0034,0x0507);
	iot_i2s_lcd_write_reg(0x0035,0x0204);
	iot_i2s_lcd_write_reg(0x0036,0x0204);
	iot_i2s_lcd_write_reg(0x0037,0x0502);
	iot_i2s_lcd_write_reg(0x003A,0x0302);
	iot_i2s_lcd_write_reg(0x003B,0x0302);
	iot_i2s_lcd_write_reg(0x0023,0x0000);
	iot_i2s_lcd_write_reg(0x0024,0x0000);
	iot_i2s_lcd_write_reg(0x0025,0x8000);
	iot_i2s_lcd_write_reg(0x004f,0);
	iot_i2s_lcd_write_reg(0x004e,0);
	
	//Arduino
// 	vTaskDelay(50 / portTICK_RATE_MS);
// 
// 	iot_i2s_lcd_write_reg(0x0000,0x0000);
// 	vTaskDelay(50 / portTICK_RATE_MS);
// 
// 	iot_i2s_lcd_write_reg(0x0000,0x0000);
// 	vTaskDelay(50 / portTICK_RATE_MS);
// 
// 	iot_i2s_lcd_write_reg(0x0000,0x0001);
// 
// 	iot_i2s_lcd_write_reg(0x0003,0xA8A4);
// 	iot_i2s_lcd_write_reg(0x000C,0x0000);
// 	iot_i2s_lcd_write_reg(0x000D,0x080C);
// 	iot_i2s_lcd_write_reg(0x000E,0x2B00);
// 	iot_i2s_lcd_write_reg(0x001E,0x00B0);
// 	iot_i2s_lcd_write_reg(0x0001,0x3B3F);
// 	vTaskDelay(50 / portTICK_RATE_MS);
// 	iot_i2s_lcd_write_reg(0x0002,0x0600);
// 	iot_i2s_lcd_write_reg(0x0010,0x0000);
// 	iot_i2s_lcd_write_reg(0x0011,0x6070);
// 	iot_i2s_lcd_write_reg(0x0005,0x0000);
// 	iot_i2s_lcd_write_reg(0x0006,0x0000);
// 	iot_i2s_lcd_write_reg(0x0016,0xEF1C);
// 	iot_i2s_lcd_write_reg(0x0017,0x0003);
// 	iot_i2s_lcd_write_reg(0x0007,0x0333);
// 	iot_i2s_lcd_write_reg(0x000B,0x0000);
// 	iot_i2s_lcd_write_reg(0x000F,0x0000);
// 	iot_i2s_lcd_write_reg(0x0041,0x0000);
// 	iot_i2s_lcd_write_reg(0x0042,0x0000);
// 	iot_i2s_lcd_write_reg(0x0048,0x0000);
// 	iot_i2s_lcd_write_reg(0x0049,0x013F);
// 	iot_i2s_lcd_write_reg(0x004A,0x0000);
// 	iot_i2s_lcd_write_reg(0x004B,0x0000);
// 	iot_i2s_lcd_write_reg(0x0044,0xEF00);
// 	iot_i2s_lcd_write_reg(0x0045,0x0000);
// 	iot_i2s_lcd_write_reg(0x0046,0x013F);
// 	iot_i2s_lcd_write_reg(0x0030,0x0707);
// 	iot_i2s_lcd_write_reg(0x0031,0x0204);
// 	iot_i2s_lcd_write_reg(0x0032,0x0204);
// 	iot_i2s_lcd_write_reg(0x0033,0x0502);
// 	iot_i2s_lcd_write_reg(0x0034,0x0507);
// 	iot_i2s_lcd_write_reg(0x0035,0x0204);
// 	iot_i2s_lcd_write_reg(0x0036,0x0204);
// 	iot_i2s_lcd_write_reg(0x0037,0x0502);
// 	iot_i2s_lcd_write_reg(0x003A,0x0302);
// 	iot_i2s_lcd_write_reg(0x003B,0x0302);
// 	iot_i2s_lcd_write_reg(0x0023,0x0000);
// 	iot_i2s_lcd_write_cmd(0x0022);
// 	gpio_set_level(13, 0);
// 	vTaskDelay(100 /portTICK_RATE_MS);
// 	gpio_set_level(13, 1);
// 	vTaskDelay(100 /portTICK_RATE_MS);
#ifdef CONFIG_DLH_LCD_TEST_LOOP
// 	uint16_t i = 0;
	while(true)
	{
		iot_i2s_lcd_write_reg(0xFFFF,0x0000);
		iot_i2s_lcd_write_reg(0x0000,0xFFFF);
// 		i++;
	}
#endif
	return true;
}

