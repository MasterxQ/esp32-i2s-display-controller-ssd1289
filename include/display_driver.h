#ifndef DPL_DISPLAY_DRIVER_H__
#define DPL_DISPLAY_DRIVER_H__ 1
#include <stddef.h>
#include <stdbool.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C"
{
#endif

typedef enum display_mem_orga_e
{
	DISPLAY_MEM_ORGA_0 = 0,
	DISPLAY_MEM_ORGA_90 = 1,
	DISPLAY_MEM_ORGA_180 = 2,
	DISPLAY_MEM_ORGA_270 = 3
} display_mem_orga_e;

extern volatile display_mem_orga_e cur_mem_orga;

bool dpl_chipset_init(void);

bool dpl_chipset_deinit(void);

void dpl_set_box(uint16_t x, uint16_t y, uint16_t x_size, uint16_t y_size);

void dpl_draw_section(void* src, uint16_t x, uint16_t y, uint16_t width, uint16_t height);

void dpl_fill_screen(uint16_t color);

void dpl_set_rotation(display_mem_orga_e mem_orga, bool force);

#ifdef __cplusplus
}
#endif
#endif
