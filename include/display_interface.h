#ifndef DPL_DISPLAY_INTERFACE_H__
#define DPL_DISPLAY_INTERFACE_H__ 1
#include <stddef.h>
#include <stdbool.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C"
{
#endif

typedef void (*display_trans_done_fptr)(void *buffer, uint32_t size);

bool dpl_interface_init(void);
void dpl_interface_cmd8(uint8_t cmd);
void dpl_interface_data8(uint8_t data);
void dpl_interface_data16(uint16_t data);
void dpl_interface_datafield16(uint16_t *data, uint32_t words, display_trans_done_fptr cb_func);
void dpl_interface_wait_empty(void);



#ifdef __cplusplus
}
#endif

#endif
