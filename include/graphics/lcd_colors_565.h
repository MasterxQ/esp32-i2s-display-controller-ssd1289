#ifndef DLH_LCD_COLORS_565_H__
#define DLH_LCD_COLORS_565_H__ 1

#include <sdkconfig.h>

#ifdef CONFIG_DLH_LCD_INTERFACE_HAS_4WIRE_SPI_BUS
//Swapping all colors because we have other endianness
#define COLOR_BLACK           0x0000
#define COLOR_BLUE            0x1F00
#define COLOR_RED             0x00F8
#define COLOR_GREEN           0xE007
#define COLOR_CYAN            0xFF07
#define COLOR_MAGENTA         0x1FF8
#define COLOR_YELLOW          0xE0FF
#define COLOR_WHITE           0xFFFF

//Additional colors
#define COLOR_NAVY            0x0F00      /*   0,   0, 128 */
#define COLOR_DARKGREEN       0xE003      /*   0, 128,   0 */
#define COLOR_DARKCYAN        0xEF03      /*   0, 128, 128 */
#define COLOR_MAROON          0x0078      /* 128,   0,   0 */
#define COLOR_PURPLE          0x0F78      /* 128,   0, 128 */
#define COLOR_OLIVE           0xE07B      /* 128, 128,   0 */
#define COLOR_LIGHTGREY       0x18C6      /* 192, 192, 192 */
#define COLOR_DARKGREY        0xEF7B      /* 128, 128, 128 */
#define COLOR_ORANGE          0x20FD      /* 255, 165,   0 */
#define COLOR_GREENYELLOW     0xE5AF      /* 173, 255,  47 */
#define COLOR_PINK            0x1FF8
#else
#define COLOR_BLACK           0x0000
#define COLOR_BLUE            0x001F
#define COLOR_RED             0xF800
#define COLOR_GREEN           0x07E0
#define COLOR_CYAN            0x07FF
#define COLOR_MAGENTA         0xF81F
#define COLOR_YELLOW          0xFFE0
#define COLOR_WHITE           0xFFFF

//Additional colors
#define COLOR_NAVY            0x000F      /*   0,   0, 128 */
#define COLOR_DARKGREEN       0x03E0      /*   0, 128,   0 */
#define COLOR_DARKCYAN        0x03EF      /*   0, 128, 128 */
#define COLOR_MAROON          0x7800      /* 128,   0,   0 */
#define COLOR_PURPLE          0x780F      /* 128,   0, 128 */
#define COLOR_OLIVE           0x7BE0      /* 128, 128,   0 */
#define COLOR_LIGHTGREY       0xC618      /* 192, 192, 192 */
#define COLOR_DARKGREY        0x7BEF      /* 128, 128, 128 */
#define COLOR_ORANGE          0xFD20      /* 255, 165,   0 */
#define COLOR_GREENYELLOW     0xAFE5      /* 173, 255,  47 */
#define COLOR_PINK            0xF81F
#endif

#endif
