#ifndef DLH_LCD_FONT_H__
#define DLH_LCD_FONT_H__ 1

#include <stdint.h>

typedef struct font_description_s {
  uint8_t width;
  uint8_t height;
	uint8_t bytes_per_col;
	uint8_t bytes_per_char;
  const uint8_t *font_data;
} font_description_s;


extern const uint8_t font0[95][5];
extern const uint8_t font1[256][20];
extern const uint8_t font2[256][24];

extern const font_description_s font_list[2];

#endif
