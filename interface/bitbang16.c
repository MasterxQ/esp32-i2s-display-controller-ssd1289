#include <display_interface.h>

#include <driver/gpio.h>

void _dpl_interface_write_bus(uint16_t data) {
	gpio_set_level(CONFIG_DLH_LCD_PIN_DB15, (data & 0x8000) >> 15);
	gpio_set_level(CONFIG_DLH_LCD_PIN_DB14, (data & 0x4000) >> 14);
	gpio_set_level(CONFIG_DLH_LCD_PIN_DB13, (data & 0x2000) >> 13);
	gpio_set_level(CONFIG_DLH_LCD_PIN_DB12, (data & 0x1000) >> 12);
	gpio_set_level(CONFIG_DLH_LCD_PIN_DB11, (data & 0x0800) >> 11);
	gpio_set_level(CONFIG_DLH_LCD_PIN_DB10, (data & 0x0400) >> 10);
	gpio_set_level(CONFIG_DLH_LCD_PIN_DB9,  (data & 0x0200) >> 9);
	gpio_set_level(CONFIG_DLH_LCD_PIN_DB8,  (data & 0x0100) >> 8);
	gpio_set_level(CONFIG_DLH_LCD_PIN_DB7,  (data & 0x0080) >> 7);
	gpio_set_level(CONFIG_DLH_LCD_PIN_DB6,  (data & 0x0040) >> 6);
	gpio_set_level(CONFIG_DLH_LCD_PIN_DB5,  (data & 0x0020) >> 5);
	gpio_set_level(CONFIG_DLH_LCD_PIN_DB4,  (data & 0x0010) >> 4);
	gpio_set_level(CONFIG_DLH_LCD_PIN_DB3,  (data & 0x0008) >> 3);
	gpio_set_level(CONFIG_DLH_LCD_PIN_DB2,  (data & 0x0004) >> 2);
	gpio_set_level(CONFIG_DLH_LCD_PIN_DB1,  (data & 0x0002) >> 1);
	gpio_set_level(CONFIG_DLH_LCD_PIN_DB0,  (data & 0x0001));
}
void _dpl_interface_send() {
	gpio_set_level(CONFIG_DLH_LCD_PIN_WR, 0);
	gpio_set_level(CONFIG_DLH_LCD_PIN_WR, 1);
}

bool dpl_interface_init(void) {
	esp_err_t ret;
	gpio_config_t config = {
		.pin_bit_mask = 
			(1ULL << CONFIG_DLH_LCD_PIN_DB0) |
			(1ULL << CONFIG_DLH_LCD_PIN_DB1) |
			(1ULL << CONFIG_DLH_LCD_PIN_DB2) |
			(1ULL << CONFIG_DLH_LCD_PIN_DB3) |
			(1ULL << CONFIG_DLH_LCD_PIN_DB4) |
			(1ULL << CONFIG_DLH_LCD_PIN_DB5) |
			(1ULL << CONFIG_DLH_LCD_PIN_DB6) |
			(1ULL << CONFIG_DLH_LCD_PIN_DB7) |
			(1ULL << CONFIG_DLH_LCD_PIN_DB8) |
			(1ULL << CONFIG_DLH_LCD_PIN_DB9) |
			(1ULL << CONFIG_DLH_LCD_PIN_DB10) |
			(1ULL << CONFIG_DLH_LCD_PIN_DB11) |
			(1ULL << CONFIG_DLH_LCD_PIN_DB12) |
			(1ULL << CONFIG_DLH_LCD_PIN_DB13) |
			(1ULL << CONFIG_DLH_LCD_PIN_DB14) |
			(1ULL << CONFIG_DLH_LCD_PIN_DB15) |
			(1ULL << CONFIG_DLH_LCD_PIN_RS) |
			(1ULL << CONFIG_DLH_LCD_PIN_WR),    /*!< GPIO pin: set with bit mask, each bit maps to a GPIO */
		.mode = GPIO_MODE_OUTPUT,               /*!< GPIO mode: set input/output mode                     */
		.pull_up_en   = GPIO_PULLUP_DISABLE,    /*!< GPIO pull-up                                         */
		.pull_down_en = GPIO_PULLDOWN_DISABLE,  /*!< GPIO pull-down                                       */
		.intr_type    =  GPIO_INTR_DISABLE      /*!< GPIO interrupt type */
	};
	ret = gpio_config(&config);

	return ret==ESP_OK;
}
void dpl_interface_cmd8(uint8_t cmd) {
	gpio_set_level(CONFIG_DLH_LCD_PIN_RS, 0);
	_dpl_interface_write_bus(cmd);
	_dpl_interface_send();
	gpio_set_level(CONFIG_DLH_LCD_PIN_RS, 1);
}
void dpl_interface_data16(uint16_t data) {
	_dpl_interface_write_bus(data);
	_dpl_interface_send();
}
void dpl_interface_data8(uint8_t data) {
	dpl_interface_data16(data);
}
void dpl_interface_datafield16(uint16_t* data, uint32_t words, display_trans_done_fptr cb_func)
{
	for (uint32_t i = 0; i < words; i++) {
		dpl_interface_data16(data[i]);
	}
	if (cb_func != NULL)
		cb_func(data);
}
