#include <display_interface.h>

#include <freertos/FreeRTOS.h>
#include <freertos/task.h>

#include <driver/spi_master.h>
#include <driver/gpio.h>


#include <string.h>
#include <esp_log.h>


#define LCD_HOST VSPI_HOST
#define DMA_CHAN 1

#define DPL_SPI_FIFO_LEN 64
#define DPL_FIFO_MODULO_MASK 0b111111

// #define DPL_SPI_MHZ 80ULL

spi_device_handle_t spi4wire_handle;

spi_transaction_t spi_fifo[DPL_SPI_FIFO_LEN];
volatile uint8_t spi_fifo_pos;



//TODO: this counters are realy dirty

// typedef enum dpl_spi_transmission_type_e
// {
// 	DPL_SPI_TRANSMISSION_TYPE_CMD = 0,
// 	DPL_SPI_TRANSMISSION_TYPE_DATA = 1
// } dpl_spi_transmission_type_e;
// 
// typedef void (*user_callback_fptr)(void *buffer, uint32_t size);

// typedef struct spi_transmit_fifo_t
// {
// 	uint8_t type;
// 	uint8_t padding[2];
// 	user_callback_fptr user_callback_func;
// 	union
// 	{
// 		uint32_t buffer32[2];
// 		uint8_t buffer8[8];
// 	};
// } spi_transmit_fifo_t;


// typedef struct dpl_spi_mem_t
// {
// 	spi_transmit_fifo_t fifo[DPL_FIFO_LENGTH];
// 	uint8_t fifo_pos;
// } dpl_spi_mem_t;

// dpl_spi_mem_t *dpl_spi_mem;



/*
 user parameter in spi is mainly used to indicate command/data
 contains 0 if a command is transmitted
 contains 1 or a user callback function if data is transmitted
 */
IRAM_ATTR void lcd_spi_pre_transfer_callback(spi_transaction_t *t)
{
	gpio_set_level(CONFIG_HTV_LCD_RS_PIN, (uint32_t)t->user != 0);
#if CONFIG_DLH_LCD_SPI_DATA_DELAY_US > 0
	if((uint32_t)t->user == 1)
		ets_delay_us(CONFIG_DLH_LCD_SPI_DATA_DELAY_US);
#endif
}

void lcd_spi_post_transfer_callback(spi_transaction_t *t)
{
// 	ESP_EARLY_LOGI("T", "u: %d s: %d", t->user, t->length >>3);
	if((uint32_t)t->user > 1)
	{
		
		((display_trans_done_fptr)t->user)((void *)t->tx_buffer, t->length >>3);
	}
}

bool dpl_interface_init(void)
{
// 	if(dpl_spi_mem == NULL)
// 	{
// 		//Need to malloc the mem because else spi can fail.
// 		dpl_spi_mem = heap_caps_malloc(sizeof(dpl_spi_mem_t), MALLOC_CAP_DMA);
// 	}
	
	
	esp_err_t ret;
	
		gpio_config_t config =
	{
		.pin_bit_mask = (1ULL << CONFIG_HTV_LCD_RS_PIN),
		.mode = GPIO_MODE_OUTPUT,
		.pull_up_en = GPIO_PULLUP_DISABLE,
		.pull_down_en = GPIO_PULLDOWN_DISABLE,
		.intr_type = GPIO_INTR_DISABLE
	};
	gpio_config(&config);

#ifdef CONFIG_HTV_LCD_USE_RESET_PIN
	config.pin_bit_mask = (1ULL << CONFIG_HTV_LCD_RESET_PIN);
	gpio_config(&config);
#endif

	memset(spi_fifo, 0, sizeof(spi_fifo));

	spi_bus_config_t buscfg={
		.miso_io_num=-CONFIG_HTV_LCD_MISO_PIN,
		.mosi_io_num=CONFIG_HTV_LCD_MOSI_PIN,
		.sclk_io_num=CONFIG_HTV_LCD_SCK_PIN,
		.quadwp_io_num=-1,
		.quadhd_io_num=-1,
		.max_transfer_sz=CONFIG_DLH_LCD_DISPLAY_SIZE_X*CONFIG_DLH_LCD_DISPLAY_SIZE_Y*2*8/12
	};
	gpio_set_drive_capability(CONFIG_HTV_LCD_MOSI_PIN, GPIO_DRIVE_CAP_1);
	gpio_set_drive_capability(CONFIG_HTV_LCD_SCK_PIN, GPIO_DRIVE_CAP_1);
	gpio_set_drive_capability(CONFIG_HTV_LCD_CS_PIN, GPIO_DRIVE_CAP_1);
	spi_device_interface_config_t devcfg={
			.clock_speed_hz=CONFIG_DLH_LCD_SPI_MHZ*1000*1000,         //Clock Speed in Hz
			.mode=0,                              //SPI mode 0
			.spics_io_num=CONFIG_HTV_LCD_CS_PIN,  //CS pin
			.flags=SPI_DEVICE_HALFDUPLEX,
			.queue_size=DPL_SPI_FIFO_LEN - 1,                        //We want to be able to queue 7 transactions at a time
			.pre_cb=&lcd_spi_pre_transfer_callback,   //Specify pre-transfer callback to handle D/C line
			.post_cb=&lcd_spi_post_transfer_callback,
	};
// 	devcfg.input_delay_ns =10;
// 	devcfg

	//Initialize the SPI bus
	ret=spi_bus_initialize(LCD_HOST, &buscfg, DMA_CHAN);
	if (ret != ESP_OK)
		return false;

	//Attach the LCD to the SPI bus
	ret=spi_bus_add_device(LCD_HOST, &devcfg, &spi4wire_handle);
#ifdef CONFIG_HTV_LCD_USE_RESET_PIN
	gpio_set_level(CONFIG_HTV_LCD_RESET_PIN, 0);
	vTaskDelay(20 / portTICK_RATE_MS);
	gpio_set_level(CONFIG_HTV_LCD_RESET_PIN, 1);
#endif
	return ret == ESP_OK;
}
void dpl_interface_cmd8(uint8_t cmd) {
// 	spi_transmit_fifo_t *fifo = dpl_spi_mem->fifo + dpl_spi_mem->fifo_pos;
// 	dpl_spi_mem->fifo_pos = (dpl_spi_mem->fifo_pos + 1) & DPL_FIFO_MODULO_MASK;
// 	fifo->buffer8[0] = cmd;
// 	fifo->type = DPL_SPI_TRANSMISSION_TYPE_CMD;
	spi_transaction_t *t = &spi_fifo[spi_fifo_pos];
	spi_fifo_pos = (spi_fifo_pos + 1) & DPL_FIFO_MODULO_MASK;

// 	memset(&t, 0, sizeof(t));       //Zero out the transaction
	t->flags = SPI_TRANS_USE_TXDATA;
	t->tx_data[0]=cmd;               //The data is the cmd itself
	t->length=8;                     //Command is 8 bits
	t->rxlength = 0;
	t->user=0;                //D/C needs to be set to 0
	
	spi_device_queue_trans(spi4wire_handle, t, portMAX_DELAY);  //Transmit!
}
void dpl_interface_data8(uint8_t data) {
	spi_transaction_t *t = &spi_fifo[spi_fifo_pos];
	spi_fifo_pos = (spi_fifo_pos + 1) & DPL_FIFO_MODULO_MASK;
	
	t->flags= SPI_TRANS_USE_TXDATA;
	t->length=8;                     //Command is 8 bits
	t->tx_data[0]=data;              //The data itself
	t->user=(void*)1;                //D/C needs to be set to 1
	t->rxlength = 0;

	spi_device_queue_trans(spi4wire_handle, t, portMAX_DELAY);  //Transmit!
}
void dpl_interface_data16(uint16_t data)
{
	spi_transaction_t *t = &spi_fifo[spi_fifo_pos];
	spi_fifo_pos = (spi_fifo_pos + 1) & DPL_FIFO_MODULO_MASK;
	
	t->flags = SPI_TRANS_USE_TXDATA,
	t->length=16;                     //Command is 8 bits
	t->user=(void*)1;                  //D/C needs to be set to 1
	t->rxlength = 0;
	*((uint16_t *)t->tx_data) = data;
	spi_device_queue_trans(spi4wire_handle, t, portMAX_DELAY);  //Transmit!
}

spi_transaction_t t_test;

void dpl_interface_datafield16(uint16_t *data, uint32_t words, display_trans_done_fptr cp_func)
{
	if(cp_func == NULL)
		cp_func = (void *)1;

	spi_transaction_t *t = &spi_fifo[spi_fifo_pos];
	spi_fifo_pos = (spi_fifo_pos + 1) & DPL_FIFO_MODULO_MASK;

	t->flags = 0;
	t->length = (words << 4);
	t->user = (void*) cp_func;
	t->tx_buffer = (void *)data;
	t->rxlength = 0;

	spi_device_queue_trans(spi4wire_handle, t, portMAX_DELAY);
}



void dpl_interface_wait_empty(void)
{
// 	spi4wire_handle
	ESP_LOGW("SPI4WIRE", "Waiting for empty not implemented");
}
