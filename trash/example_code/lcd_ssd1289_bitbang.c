#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/gpio.h"
#include "sdkconfig.h"

#ifdef COMMENDIN


void lcd_write_bus(uint16_t data){
	gpio_set_level(CONFIG_DLH_LCD_PIN_DB15, (data & 0x8000) >> 15);
	gpio_set_level(CONFIG_DLH_LCD_PIN_DB14, (data & 0x4000) >> 14);
	gpio_set_level(CONFIG_DLH_LCD_PIN_DB13, (data & 0x2000) >> 13);
	gpio_set_level(CONFIG_DLH_LCD_PIN_DB12, (data & 0x1000) >> 12);
	gpio_set_level(CONFIG_DLH_LCD_PIN_DB11, (data & 0x0800) >> 11);
	gpio_set_level(CONFIG_DLH_LCD_PIN_DB10, (data & 0x0400) >> 10);
	gpio_set_level(CONFIG_DLH_LCD_PIN_DB9,  (data & 0x0200) >> 9);
	gpio_set_level(CONFIG_DLH_LCD_PIN_DB8,  (data & 0x0100) >> 8);
	gpio_set_level(CONFIG_DLH_LCD_PIN_DB7,  (data & 0x0080) >> 7);
	gpio_set_level(CONFIG_DLH_LCD_PIN_DB6,  (data & 0x0040) >> 6);
	gpio_set_level(CONFIG_DLH_LCD_PIN_DB5,  (data & 0x0020) >> 5);
	gpio_set_level(CONFIG_DLH_LCD_PIN_DB4,  (data & 0x0010) >> 4);
	gpio_set_level(CONFIG_DLH_LCD_PIN_DB3,  (data & 0x0008) >> 3);
	gpio_set_level(CONFIG_DLH_LCD_PIN_DB2,  (data & 0x0004) >> 2);
	gpio_set_level(CONFIG_DLH_LCD_PIN_DB1,  (data & 0x0002) >> 1);
	gpio_set_level(CONFIG_DLH_LCD_PIN_DB0,  (data & 0x0001));
}
void delay_ms(uint16_t ms){
	vTaskDelay(ms / portTICK_PERIOD_MS);
}
void lcd_write_data(uint16_t data){
	gpio_set_level(CONFIG_DLH_LCD_PIN_RS, 1);
	lcd_write_bus(data);
	gpio_set_level(CONFIG_DLH_LCD_PIN_WR, 0);
	gpio_set_level(CONFIG_DLH_LCD_PIN_WR, 1);
}
void lcd_write_com(uint16_t cmd){
	gpio_set_level(CONFIG_DLH_LCD_PIN_RS, 0);
	lcd_write_bus(cmd);
	gpio_set_level(CONFIG_DLH_LCD_PIN_WR, 0);
	gpio_set_level(CONFIG_DLH_LCD_PIN_WR, 1);
}
void lcd_write_com_data(uint16_t cmd, uint16_t data){
	lcd_write_com(cmd);
	lcd_write_data(data);
}
void setup_gpio(){
	gpio_config_t config = {
	    .pin_bit_mask = 
				(1ULL << CONFIG_DLH_LCD_PIN_DB0) |
				(1ULL << CONFIG_DLH_LCD_PIN_DB1) |
				(1ULL << CONFIG_DLH_LCD_PIN_DB2) |
				(1ULL << CONFIG_DLH_LCD_PIN_DB3) |
				(1ULL << CONFIG_DLH_LCD_PIN_DB4) |
				(1ULL << CONFIG_DLH_LCD_PIN_DB5) |
				(1ULL << CONFIG_DLH_LCD_PIN_DB6) |
				(1ULL << CONFIG_DLH_LCD_PIN_DB7) |
				(1ULL << CONFIG_DLH_LCD_PIN_DB8) |
				(1ULL << CONFIG_DLH_LCD_PIN_DB9) |
				(1ULL << CONFIG_DLH_LCD_PIN_DB10) |
				(1ULL << CONFIG_DLH_LCD_PIN_DB11) |
				(1ULL << CONFIG_DLH_LCD_PIN_DB12) |
				(1ULL << CONFIG_DLH_LCD_PIN_DB13) |
				(1ULL << CONFIG_DLH_LCD_PIN_DB14) |
				(1ULL << CONFIG_DLH_LCD_PIN_DB15) |
				(1ULL << CONFIG_DLH_LCD_PIN_RS) |
				(1ULL << CONFIG_DLH_LCD_PIN_WR),          /*!< GPIO pin: set with bit mask, each bit maps to a GPIO */
      .mode = GPIO_MODE_OUTPUT,               /*!< GPIO mode: set input/output mode                     */
      .pull_up_en = GPIO_PULLUP_DISABLE,       /*!< GPIO pull-up                                         */
      .pull_down_en = GPIO_PULLDOWN_DISABLE,   /*!< GPIO pull-down                                       */
      .intr_type =  GPIO_INTR_DISABLE          /*!< GPIO interrupt type */
	};
	gpio_config(&config);

	gpio_set_level(CONFIG_DLH_LCD_PIN_RS, 0);
	gpio_set_level(CONFIG_DLH_LCD_PIN_WR, 1);
	lcd_write_bus(0);
}


void lcd_init(){
	/*
	lcd_write_com_data(0x0000,0x0001);
	lcd_write_com_data(0x0003,0xA8A4);
	lcd_write_com_data(0x000C,0x0000);
	lcd_write_com_data(0x000D,0x080C);
	lcd_write_com_data(0x000E,0x2B00);
	lcd_write_com_data(0x001E,0x00B0);
	lcd_write_com_data(0x0001,0x3B3F);
	delay_ms(50);
	lcd_write_com_data(0x0002,0x0600);
	lcd_write_com_data(0x0010,0x0000);
	lcd_write_com_data(0x0011,0x6070);
	lcd_write_com_data(0x0005,0x0000);
	lcd_write_com_data(0x0006,0x0000);
	lcd_write_com_data(0x0016,0xEF1C);
	lcd_write_com_data(0x0017,0x0003);
	lcd_write_com_data(0x0007,0x0333);
	lcd_write_com_data(0x000B,0x0000);
	lcd_write_com_data(0x000F,0x0000);
	lcd_write_com_data(0x0041,0x0000);
	lcd_write_com_data(0x0042,0x0000);
	lcd_write_com_data(0x0048,0x0000);
	lcd_write_com_data(0x0049,0x013F);
	lcd_write_com_data(0x004A,0x0000);
	lcd_write_com_data(0x004B,0x0000);
	lcd_write_com_data(0x0044,0xEF00);
	lcd_write_com_data(0x0045,0x0000);
	lcd_write_com_data(0x0046,0x013F);
	lcd_write_com_data(0x0030,0x0707);
	lcd_write_com_data(0x0031,0x0204);
	lcd_write_com_data(0x0032,0x0204);
	lcd_write_com_data(0x0033,0x0502);
	lcd_write_com_data(0x0034,0x0507);
	lcd_write_com_data(0x0035,0x0204);
	lcd_write_com_data(0x0036,0x0204);
	lcd_write_com_data(0x0037,0x0502);
	lcd_write_com_data(0x003A,0x0302);
	lcd_write_com_data(0x003B,0x0302);
	lcd_write_com_data(0x0023,0x0000);
	lcd_write_com(0x0022); */
	lcd_write_com_data(0x0000,0x0001);
// 	printf("first command done\n");
	vTaskDelay(10 / portTICK_RATE_MS);
	lcd_write_com_data(0x0003,0xA8A4);
	lcd_write_com_data(0x000C,0x0000);
	lcd_write_com_data(0x000D,0x080C);
	lcd_write_com_data(0x000E,0x2B00);
	lcd_write_com_data(0x001E,0x00B0);
							  //  0RRCBSTMMMMMMMMM  // Change C to swap odd and even
	lcd_write_com_data(0x0001,0b0011101100111111);    //vTaskDelay(50 / portTICK_RATE_MS);   /* 320*240 0x2B3F */
	lcd_write_com_data(0x0002,0x0600);
	lcd_write_com_data(0x0010,0x0000);
	lcd_write_com_data(0x0011,0x6070);
	lcd_write_com_data(0x0005,0x0000);
	lcd_write_com_data(0x0006,0x0000);
	lcd_write_com_data(0x0016,0xEF1C);
	lcd_write_com_data(0x0017,0x0003);
	lcd_write_com_data(0x0007,0x0333);      
	lcd_write_com_data(0x000B,0x0000);
	lcd_write_com_data(0x000F,0x0000);
	lcd_write_com_data(0x0041,0x0000);
	lcd_write_com_data(0x0042,0x0000);
	lcd_write_com_data(0x0048,0x0000);
	lcd_write_com_data(0x0049,0x013F);
	lcd_write_com_data(0x004A,0x0000);
	lcd_write_com_data(0x004B,0x0000);
	lcd_write_com_data(0x0044,0xEF00);
	lcd_write_com_data(0x0045,0x0000);
	lcd_write_com_data(0x0046,0x013F);
	lcd_write_com_data(0x0030,0x0707);
	lcd_write_com_data(0x0031,0x0204);
	lcd_write_com_data(0x0032,0x0204);
	lcd_write_com_data(0x0033,0x0502);
	lcd_write_com_data(0x0034,0x0507);
	lcd_write_com_data(0x0035,0x0204);
	lcd_write_com_data(0x0036,0x0204);
	lcd_write_com_data(0x0037,0x0502);
	lcd_write_com_data(0x003A,0x0302);
	lcd_write_com_data(0x003B,0x0302);
	lcd_write_com_data(0x0023,0x0000);
	lcd_write_com_data(0x0024,0x0000);
	lcd_write_com_data(0x0025,0x8000);
	lcd_write_com_data(0x004f,0);
	lcd_write_com_data(0x004e,0);
}
void setPixel(uint16_t x, uint16_t y, uint16_t color){
	lcd_write_com_data(0x0044,x);
	lcd_write_com_data(0x0045,y);
	lcd_write_com_data(0x0046,y);
	lcd_write_com_data(0x004e,x);
	lcd_write_com_data(0x004f,y);
	lcd_write_com(0x0022);
	lcd_write_data(color);
}
void lcd_fill(uint16_t color){
	for(uint16_t x = 0; x < 240; x++){
		for(uint16_t y = 0; y < 320; y++){
			setPixel(x,y,color);
		}
	}
}
void lcd_fillr(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2, uint16_t color){
    lcd_write_com_data(0x0044, (x2 << 8) + x1);
    lcd_write_com_data(0x0045, y1);
    lcd_write_com_data(0x0046, y2);
    lcd_write_com_data(0x004E, x1);
    lcd_write_com_data(0x004F, y1);
    lcd_write_com(0x0022);
    for(uint16_t x = x1; x <= x2; x++){
        for(uint16_t y = y1; y <= y2; y++){
            lcd_write_data(color);
        }
    }
}

void lcd_just_data(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2, uint16_t color)
{
	for(uint16_t x = x1; x <= x2; x++){
		for(uint16_t y = y1; y <= y2; y++){
			lcd_write_data(color);
		}
	}
}

void easy_test(void)
{
	setup_gpio();
	lcd_init();
	printf("Running\n");
	int i = 0;
	while (1) {
		printf("%d\n",i);
        printf("White\n");
        lcd_fillr(0,0,239,319,0b1111111111111111);
        printf("Magenta\n");
        lcd_fillr(0,0,239,319,0b1111100000011111);
        printf("Cyan\n");
        lcd_fillr(0,0,239,319,0b0000011111111111);
        printf("Yellow\n");
        lcd_fillr(0,0,239,319,0b1111111111100000);
        printf("Blue\n");
        lcd_fillr(0,0,239,319,0b0000000000011111);
        printf("Green\n");
        lcd_fillr(0,0,239,319,0b0000011111100000);
        printf("Red\n");
        lcd_fillr(0,0,239,319,0b1111100000000000);
        printf("Black\n");
        lcd_fillr(0,0,239,319,0b0000000000000000);
        //lcd_fillr(10, 10, i, 50, c);
        //c = ~c;
        delay_ms(200);
		/*printf("LSB 0\n");
		lcd_fill(0b0000000000000001);
		printf("1\n");
		lcd_fill(0b0000000000000011);
		printf("2\n");
		lcd_fill(0b0000000000000111);
		printf("3\n");
		lcd_fill(0b0000000000001111);
		printf("4\n");
		lcd_fill(0b0000000000011111);
		printf("5\n");
		lcd_fill(0b0000000000111111);
		printf("6\n");
		lcd_fill(0b0000000001111111);
		printf("7\n");
		lcd_fill(0b0000000011111111);
		printf("8\n");
		lcd_fill(0b0000000111111111);
		printf("9\n");
		lcd_fill(0b0000001111111111);
		printf("10\n");
		lcd_fill(0b0000011111111111);
		printf("11\n");
		lcd_fill(0b0000111111111111);
		printf("12\n");
		lcd_fill(0b0001111111111111);
		printf("13\n");
		lcd_fill(0b0011111111111111);
		printf("14\n");
		lcd_fill(0b0111111111111111);
		printf("15\n");
		lcd_fill(0b1111111111111111);*/
		i++;
		//setPixel(10,i,0b1111100000011111);
	}
}
#endif
