#include <stdio.h>
#include <lcd_i2s_com.h>
#include "iot_i2s_lcd.h"
#include <lcd_i2s.h>
#include <lcd_i2s_text.h>
// #include "lcd_ssd1289_bitbang.h"
#include <esp_timer.h>
// #include <lcd_gui.hpp>
// #include

#include "../components/lcd_i2s/example_code/test_image.c"

void app_main(void)
{
	vTaskDelay(199/ portTICK_RATE_MS);
	esp_timer_init();
// 	easy_test();
// 	printf("Testing printf");
	gpio_config_t config = {
		.pin_bit_mask = (1 << 12) | (1 << 13) | (1 << 14) | (1 << 15),          /*!< GPIO pin: set with bit mask, each bit maps to a GPIO */
		.mode = GPIO_MODE_OUTPUT,               /*!< GPIO mode: set input/output mode                     */
		.pull_up_en = GPIO_PULLUP_DISABLE,       /*!< GPIO pull-up                                         */
		.pull_down_en = GPIO_PULLDOWN_DISABLE,   /*!< GPIO pull-down                                       */
		.intr_type =  GPIO_INTR_DISABLE          /*!< GPIO interrupt type */
	};
	gpio_config(&config);
	gpio_set_direction(12, GPIO_MODE_OUTPUT);
	
	gpio_set_level(13, 1);
	vTaskDelay(199/ portTICK_RATE_MS);
	
	if(!lcd_init())
	{
		printf("Display init fail\n");
		goto end;
	}
	printf("Filling screen with cyan\n");
// 	lcd_ssd1289_set_box(0,0,240,320);
// 	setup_gpio();
// 	while(true)
// 	while (1) {
// // 		lcd_ssd1289_set_box(0,0,240,320);
// 		printf("White\n");
// 		lcd_just_data(0,0,239,319,0b1111111111111111);
// 		printf("Magenta\n");
// 		lcd_just_data(0,0,239,319,0b1111100000011111);
// 		printf("Cyan\n");
// 		lcd_just_data(0,0,239,319,0b0000011111111111);
// 		printf("Yellow\n");
// 		lcd_just_data(0,0,239,319,0b1111111111100000);
// 		printf("Blue\n");
// 		lcd_just_data(0,0,239,319,0b0000000000011111);
// 		printf("Green\n");
// 		lcd_just_data(0,0,239,319,0b0000011111100000);
// 		printf("Red\n");
// 		lcd_just_data(0,0,239,319,0b1111100000000000);
// 		printf("Black\n");
// 		lcd_just_data(0,0,239,319,0b0000000000000000);
// 		//lcd_fillr(10, 10, i, 50, c);
// 		//c = ~c;
// 		vTaskDelay(100 / portTICK_PERIOD_MS);
// 	}
// 	lcd_ssd1289_fill_screen(COLOR_BLUE);

	lcd_set_box(0,0,240,320);
	for(uint16_t y = 0; y < 320; y++)
	{
		for(uint8_t x = 0; x < 240; x++)
		{
// 			((uint16_t *)(ssd1289_device.lcd_buf))[y * 240 + x] = action_bois[y][x];
			iot_i2s_lcd_write_data(action_bois[y][x]);
		}
	}
// 	lcd_refresh();
	
	
	print_text("MasterQ & Lars!", 20, 255, 0, 240, COLOR_BLACK, COLOR_PINK, 1, 1);
// 	LcdGui::initLcdGui(0, 0, 320, 240, 4, 3, NULL, 4);
	while (true)
		vTaskDelay(1000 / portTICK_RATE_MS);

	while(true)
	{
// 		lcd_set_box(0,0,240,320);
// 		for(uint8_t i = 0; i < 8; i++)
// 		{
// 			
// // 			printf("done\n");
// // 			ets_delay_us(1000);
// 			uint32_t wordpos = 0;
// 			uint16_t color_arr[8] = {COLOR_YELLOW, COLOR_BLUE, COLOR_CYAN, COLOR_DARKGREEN, COLOR_LIGHTGREY, COLOR_ORANGE, COLOR_PINK, COLOR_BLACK};
// 			uint16_t color = color_arr[i];
// 			for(uint16_t x = 0; x < 160; x++)
// 			{
// 				for(uint16_t y = 0; y < 240; y++)
// 				{
// 					((uint16_t *)(ssd1289_device.lcd_buf))[wordpos] = color;
// 	// 				color = ~color;
// 	// 				iot_i2s_lcd_write_data(ssd1289_device.i2s_lcd_handle, color);
// 					wordpos++;
// 				}
// 				for(uint16_t y = 0; y < 240; y++)
// 				{
// 					((uint16_t *)(ssd1289_device.lcd_buf))[wordpos] = ~color;
// 	// 				color = ~color;
// 	// 				iot_i2s_lcd_write_data(ssd1289_device.i2s_lcd_handle, color);
// 					wordpos++;
// 				}
// 			}
// 			int64_t time = esp_timer_get_time();
// 			lcd_refresh();
// // 			ESP_LOGI("TIMING", "Frame took %llu s", esp_timer_get_time() - time);
// 			
// 			vTaskDelay(1000 / portTICK_RATE_MS);
// 
// 		}
	}
// 	while(true)
// 	{
// 				uint32_t wordpos = 0;
// 				uint16_t color = 0xAAAA;
// 				for(uint16_t x = 0; x < 240; x++)
// 				{
// 					for(uint16_t y = 0; y < 320; y++)
// 					{
// 						((uint16_t *)(ssd1289_device.lcd_buf))[wordpos] = color;
// 						color = ~color;
// 						iot_i2s_lcd_write_data(ssd1289_device.i2s_lcd_handle, color);
// 						wordpos++;
// 					}
// 				}
// 	}
// 	uint8_t test = 0;
// 	while(true)
// 	{
// 		iot_i2s_lcd_write_data(ssd1289_device.i2s_lcd_handle, test);
// 		iot_i2s_lcd_write_cmd(ssd1289_device.i2s_lcd_handle, test);
// 		iot_i2s_lcd_write_reg(ssd1289_device.i2s_lcd_handle, test, test);
// 		test++;
// 	}
// 	while(true)
// 	{
// 		lcd_ssd1289_set_box(0,0,240,320);
// 		iot_i2s_lcd_write_data(ssd1289_device.i2s_lcd_handle, 0x0000);
// 		
// 		lcd_ssd1289_refresh();
// 		ets_delay_us(100);
// 	}

	printf("Refreshing screen\n");

	printf("Done\n");
	end:
	while(true)
	{
		vTaskDelay(10/portTICK_RATE_MS);
	}
}
