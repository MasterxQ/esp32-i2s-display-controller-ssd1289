#ifndef DLH_SSD1289_BITBANG_H__
#define DLH_SSD1289_BITBANG_H__

#include <stdint.h>

void easy_test(void);

void setup_gpio();

void lcd_fillr(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2, uint16_t color);

void lcd_just_data(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2, uint16_t color);


#endif
