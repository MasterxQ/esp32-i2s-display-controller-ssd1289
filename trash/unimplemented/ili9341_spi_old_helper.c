
#include <lcd_i2s.h>

#include "stdio.h"
#include "string.h"
#include "freertos/FreeRTOS.h"
#include "esp_system.h"
#include <lcd_i2s_com.h>
// #include "include/lcd_ssd1289.h"
#include "sdkconfig.h"

#include "driver/spi_master.h"
#include "driver/gpio.h"

#define LCD_HOST    HSPI_HOST
#define DMA_CHAN    2
spi_device_handle_t spi;

void lcd_cmd(const uint8_t cmd)
{
    esp_err_t ret;
    spi_transaction_t t;
    memset(&t, 0, sizeof(t));       //Zero out the transaction
    t.length=8;                     //Command is 8 bits
    t.tx_buffer=&cmd;               //The data is the cmd itself
    t.user=(void*)0;                //D/C needs to be set to 0
    ret=spi_device_polling_transmit(spi, &t);  //Transmit!
    assert(ret==ESP_OK);            //Should have had no issues.
}
void lcd_data8(const uint8_t data)
{
    esp_err_t ret;
    spi_transaction_t t;
    memset(&t, 0, sizeof(t));       //Zero out the transaction
    t.length=8;                     //Command is 8 bits
    t.tx_buffer=&data;               //The data is the cmd itself
    t.user=(void*)1;                //D/C needs to be set to 0
    ret=spi_device_polling_transmit(spi, &t);  //Transmit!
    assert(ret==ESP_OK);            //Should have had no issues.
}
// void lcd_data16(const uint16_t data){
//     lcd_data8(data >> 8);
//     lcd_data8(data);
// }
void lcd_data16(uint16_t data)
{
    data = SPI_SWAP_DATA_TX(data, 16);
    esp_err_t ret;
    spi_transaction_t t;
    memset(&t, 0, sizeof(t));       //Zero out the transaction
    t.length=16;                     //Command is 16 bits
    t.tx_buffer=&data;               //The data is the cmd itself
    t.user=(void*)1;                //D/C needs to be set to 0
    ret=spi_device_polling_transmit(spi, &t);  //Transmit!
    assert(ret==ESP_OK);            //Should have had no issues.
}
void lcd_datafield(const uint8_t *data, int len)
{
    esp_err_t ret;
    spi_transaction_t t;
    if (len==0) return;             //no need to send anything
    memset(&t, 0, sizeof(t));       //Zero out the transaction
    t.length=len*8;                 //Len is in bytes, transaction length is in bits.
    t.tx_buffer=data;               //Data
    t.user=(void*)1;                //D/C needs to be set to 1
    ret=spi_device_polling_transmit(spi, &t);  //Transmit!
    assert(ret==ESP_OK);            //Should have had no issues.
}
void lcd_spi_pre_transfer_callback(spi_transaction_t *t)
{
    int dc=(int)t->user;
    gpio_set_level(CONFIG_DLH_LCD_PIN_RS, dc);
}

void lcd_set_box(uint16_t x, uint16_t y, uint16_t x_size, uint16_t y_size)
{
	uint16_t x_end = x + (x_size - 1);
	uint16_t y_end = y + (y_size - 1);
	lcd_cmd(0x2A); //column
	lcd_data16(x);
	lcd_data16(x_end);
	lcd_cmd(0x2B); //page
	lcd_data16(y);
	lcd_data16(y_end);
	lcd_cmd(0x2C); //write
}

void lcd_draw_section ( void* src, uint16_t x, uint16_t y, uint16_t width, uint16_t height )
{
// 	ESP_LOGI("DRAW", "setting area (%d,%d,%d,%d)",x,y,width,height);
	lcd_set_box(x, y, width, height);
// 	i2s_iot_i2s_lcd_write_data8(src, width * height * 2, 100/portTICK_RATE_MS, false);
	for(uint32_t i = 0; i < width * height; i++)
	{
		iot_i2s_lcd_write_data(*(((uint16_t *)src) +i));
	}
}

void lcd_fill_screen(uint16_t col) {
//     uint16_t buffer[80*80/2 + 4];
//     uint32_t tmp = (uint32_t) buffer;
//     tmp+=3;
//     tmp &= ~0b11;
    uint16_t *buffer_ptr = heap_caps_malloc(80*80*2/2, MALLOC_CAP_DMA);
    for(uint16_t i = 0; i < 80*80/2; i++){
        buffer_ptr[i] = col;
    }
    lcd_set_box(0,0,240,320);
    for(uint8_t i = 0; i < 12*2; i++)
	{
        lcd_datafield((void*)buffer_ptr, 80*80*2/2);
    }
    free(buffer_ptr);
}

void lcd_refresh(void)
{
//     iot_i2s_lcd_write(ssd1289_device.lcd_buf, CONFIG_DLH_LCD_DISPLAY_SIZE_X * CONFIG_DLH_LCD_DISPLAY_SIZE_Y * LCD_SSD1289_PIXEL_SIZE);
}

void lcd_draw_bmp(uint16_t *bmp, uint16_t x, uint16_t y, uint16_t x_size, uint16_t y_size)
{
}



bool dpl_chipset_init(void)
{
    printf("hallo\n");
    esp_err_t ret;
    spi_bus_config_t buscfg={
        .miso_io_num=-1,
        .mosi_io_num=CONFIG_DLH_LCD_PIN_MOSI,
        .sclk_io_num=CONFIG_DLH_LCD_PIN_SCK,
        .quadwp_io_num=-1,
        .quadhd_io_num=-1,
        .max_transfer_sz=240*320*2*8/12
    };
    
    spi_device_interface_config_t devcfg={
        .clock_speed_hz=40*1000*1000,            //Clock out at 10 MHz
        .mode=0,                                //SPI mode 0
        .spics_io_num=CONFIG_DLH_LCD_PIN_CS,    //CS pin
        .flags=SPI_DEVICE_HALFDUPLEX,
        .queue_size=7,                          //We want to be able to queue 7 transactions at a time
        .pre_cb=lcd_spi_pre_transfer_callback,  //Specify pre-transfer callback to handle D/C line
    };
    //Initialize the SPI bus
    ret=spi_bus_initialize(LCD_HOST, &buscfg, DMA_CHAN);
    printf("huiii\n");
    ESP_ERROR_CHECK(ret);
    //Attach the LCD to the SPI bus
    ret=spi_bus_add_device(LCD_HOST, &devcfg, &spi);
    printf("yea\n");
    ESP_ERROR_CHECK(ret);
    
    gpio_pad_select_gpio(CONFIG_DLH_LCD_PIN_RS);
    gpio_set_direction(CONFIG_DLH_LCD_PIN_RS, GPIO_MODE_OUTPUT);
#ifdef CONFIG_DLH_LCD_USE_RESET
    gpio_pad_select_gpio(CONFIG_DLH_LCD_PIN_RESET);
    gpio_set_direction(CONFIG_DLH_LCD_PIN_RESET, GPIO_MODE_OUTPUT);
	gpio_set_level(CONFIG_DLH_LCD_PIN_RESET, false);
	vTaskDelay(50 / portTICK_RATE_MS);
	gpio_set_level(CONFIG_DLH_LCD_PIN_RESET, true);
#endif
// 	iot_i2s_lcd_write_cmd(i2s_lcd_handle, 0x01);
	printf("Starting display init in 10ms\n");
	vTaskDelay(500 / portTICK_RATE_MS);

	lcd_cmd(0xcf); 
	lcd_data8(0x00);
	lcd_data8(0xc1);
	lcd_data8(0x30);

	lcd_cmd(0xed); 
	lcd_data8(0x64);
	lcd_data8(0x03);
	lcd_data8(0x12);
	lcd_data8(0x81);

	lcd_cmd(0xcb); 
	lcd_data8(0x39);
	lcd_data8(0x2c);
	lcd_data8(0x00);
	lcd_data8(0x34);
	lcd_data8(0x02);

	lcd_cmd(0xea); 
	lcd_data8(0x00);
	lcd_data8(0x00);

	lcd_cmd(0xe8); 
	lcd_data8(0x85);
	lcd_data8(0x10);
	lcd_data8(0x79);

	lcd_cmd(0xC0); //Power control
	lcd_data8(0x23); //VRH[5:0]

	lcd_cmd(0xC1); //Power control
	lcd_data8(0x11); //SAP[2:0];BT[3:0]

	lcd_cmd(0xC2);
	lcd_data8(0x11);

	lcd_cmd(0xC5); //VCM control
	lcd_data8(0x3d);
	lcd_data8(0x30);

	lcd_cmd(0xc7); 
	lcd_data8(0xaa);

	lcd_cmd(0x3A); 
	lcd_data8(0x55);

	lcd_cmd(0x36); // Memory Access Control
	lcd_data8(0x08);

	lcd_cmd(0xB1); // Frame Rate Control
	lcd_data8(0x00);
	lcd_data8(0x11);

	lcd_cmd(0xB6); // Display Function Control
	lcd_data8(0x0a);
	lcd_data8(0xa2);

	lcd_cmd(0xF2); // 3Gamma Function Disable
	lcd_data8(0x00);

	lcd_cmd(0xF7);
	lcd_data8(0x20);

	lcd_cmd(0xF1);
	lcd_data8(0x01);
	lcd_data8(0x30);

	lcd_cmd(0x26); //Gamma curve selected
	lcd_data8(0x01);

	lcd_cmd(0xE0); //Set Gamma
	lcd_data8(0x0f);
	lcd_data8(0x3f);
	lcd_data8(0x2f);
	lcd_data8(0x0c);
	lcd_data8(0x10);
	lcd_data8(0x0a);
	lcd_data8(0x53);
	lcd_data8(0xd5);
	lcd_data8(0x40);
	lcd_data8(0x0a);
	lcd_data8(0x13);
	lcd_data8(0x03);
	lcd_data8(0x08);
	lcd_data8(0x03);
	lcd_data8(0x00);

	lcd_cmd(0xE1); //Set Gamma
	lcd_data8(0x00);
	lcd_data8(0x00);
	lcd_data8(0x10);
	lcd_data8(0x03);
	lcd_data8(0x0f);
	lcd_data8(0x05);
	lcd_data8(0x2c);
	lcd_data8(0xa2);
	lcd_data8(0x3f);
	lcd_data8(0x05);
	lcd_data8(0x0e);
	lcd_data8(0x0c);
	lcd_data8(0x37);
	lcd_data8(0x3c);
	lcd_data8(0x0F);
//     lcd_cmd(0xF6);
//     lcd_data8(0b00000001);
//     lcd_data8(0b00100000);
//     lcd_data8(0);
	lcd_cmd(0x11); //Exit Sleep
	vTaskDelay(120 / portTICK_RATE_MS);
	lcd_cmd(0x29); //display on
	lcd_cmd(0x2c); //display on
	vTaskDelay(50 / portTICK_RATE_MS);
    
    return true;
}
