// Copyright 2015-2016 Espressif Systems (Shanghai) PTE LTD
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#include <string.h>
#include <math.h>
#include <esp_types.h>

#include "freertos/FreeRTOS.h"
#include "freertos/queue.h"
#include "freertos/xtensa_api.h"
#include "freertos/semphr.h"
#include "soc/dport_reg.h"
#include "esp32/rom/lldesc.h"
#include "driver/gpio.h"
#include "include/iot_i2s_lcd.h"
#include "esp_intr_alloc.h"
#include "esp_err.h"
#include "esp_log.h"

#include "lcd_i2s_helper.h"

#include "sdkconfig.h"


#define I2S_CHECK(a, str, ret) if (!(a)) {                                              \
        ESP_LOGE(I2S_TAG,"%s:%d (%s):%s", __FILE__, __LINE__, __FUNCTION__, str);       \
        return (ret);                                                                   \
        }
#define I2S_MAX_BUFFER_SIZE (4 * 1024 * 1024) //the maximum RAM can be allocated
#define I2S_BASE_CLK (2*APB_CLK_FREQ)
// #define I2S_ENTER_CRITICAL_ISR()     portENTER_CRITICAL_ISR(&i2s_spinlock[i2s_num])
// #define I2S_EXIT_CRITICAL_ISR()      portEXIT_CRITICAL_ISR(&i2s_spinlock[i2s_num])
// #define I2S_ENTER_CRITICAL()         portENTER_CRITICAL(&i2s_spinlock[i2s_num])
// #define I2S_EXIT_CRITICAL()          portEXIT_CRITICAL(&i2s_spinlock[i2s_num])

//This macro definition only for lcd and camera mode.
//In i2s dma link descriptor, the maximum of dma buffer is 4095 bytes,
//so we take the length of 1023 words here.
// #define DMA_SIZE (2000)  //words
/**
 * @brief DMA buffer object
 *
 */
typedef struct {
    char **buf;
    int buf_size;
    int rw_pos;
    void *curr_ptr;
    SemaphoreHandle_t mux;
    xQueueHandle queue;
    lldesc_t **desc;
} i2s_dma_t;

#define CONFIG_DLH_DMA_NUM_BUFS 1
#define CONFIG_DHL_DMA_WORD_SIZE 1000

/**
 * @brief I2S object instance
 *
 */
typedef struct {
    int queue_size;                  /*!< I2S event queue size*/
    QueueHandle_t i2s_queue;         /*!< I2S queue handler*/
//     int dma_buf_count;               /*!< DMA buffer count, number of buffer*/
//     int dma_buf_len;                 /*!< DMA buffer length, length of each buffer*/
    i2s_dma_t *rx;                   /*!< DMA Tx buffer*/
    i2s_dma_t *tx;                   /*!< DMA Rx buffer*/
    i2s_isr_handle_t i2s_isr_handle; /*!< I2S Interrupt handle*/
    int channel_num;                 /*!< Number of channels*/
    int bytes_per_sample;            /*!< Bytes per sample*/
    int bits_per_sample;             /*!< Bits per sample*/
    i2s_mode_t mode;                 /*!< I2S Working mode*/
    uint32_t sample_rate;            /*!< I2S sample rate */
    bool use_apll;                   /*!< I2S use APLL clock */
    int fixed_mclk;                  /*!< I2S fixed MLCK clock */
} i2s_obj_t;

static const char *I2S_TAG = "IOT_I2S";
static i2s_obj_t *p_i2s_obj[I2S_NUM_MAX] = {0};

// static portMUX_TYPE i2s_spinlock[I2S_NUM_MAX] = {portMUX_INITIALIZER_UNLOCKED, portMUX_INITIALIZER_UNLOCKED};

static esp_err_t i2s_isr_register(int intr_alloc_flags, void (*fn)(void *), void *arg, i2s_isr_handle_t *handle)
{
    return esp_intr_alloc(ETS_I2S0_INTR_SOURCE + CONFIG_DLH_LCD_I2S_DEVICE_NUM, intr_alloc_flags, fn, arg, handle);
}

static esp_err_t i2s_set_parallel_mode()
{
	i2s_dma_t *dma = NULL;
	if ((dma = (i2s_dma_t *)malloc(sizeof(i2s_dma_t))) == NULL) {
		ESP_LOGE(I2S_TAG, "malloc i2s_dma_t fail");
		return ESP_FAIL;
	}
	if ((dma->desc = (lldesc_t **)malloc(sizeof(lldesc_t *))) == NULL) {
		ESP_LOGE(I2S_TAG, "malloc lldesc_t* fail");
		goto _err;
	}
	if ((*(dma->desc) = (lldesc_t *)malloc(sizeof(lldesc_t))) == NULL) {
		ESP_LOGE(I2S_TAG, "malloc lldesc_t fail");
		goto _err;
	}
	memset(*(dma->desc), 0, sizeof(lldesc_t));
	(*(dma->desc))->sosf = 1;
	(*(dma->desc))->eof = 1;
	(*(dma->desc))->owner = 1;
	if ((dma->buf = (char **)malloc(sizeof(char *) * 2)) == NULL) {
		ESP_LOGE(I2S_TAG, "malloc dma buf fail");
		goto _err;
	}

	// In lcd mode, we should allocate a buffer for dma, and configure clk for lcd mode.
	char *buff = (char *)malloc(sizeof(uint16_t) * 2 * CONFIG_DHL_DMA_WORD_SIZE);
	if (buff == NULL) {
		ESP_LOGE(I2S_TAG, "malloc dma buf fail");
		goto _err;
	}
	memset(buff, 0, sizeof(uint16_t) * 2 * CONFIG_DHL_DMA_WORD_SIZE);
	dma->buf[0] = buff;
	dma->buf[1] = buff + CONFIG_DHL_DMA_WORD_SIZE * 2;
	(*(dma->desc))->buf = (uint8_t *)(dma->buf[0]);
	dma->buf_size = CONFIG_DHL_DMA_WORD_SIZE;
	p_i2s_obj[CONFIG_DLH_LCD_I2S_DEVICE_NUM]->tx = dma;
// 	p_i2s_obj[CONFIG_DLH_LCD_I2S_DEVICE_NUM]->dma_buf_count = 1;
// 	p_i2s_obj[CONFIG_DLH_LCD_I2S_DEVICE_NUM]->dma_buf_len = sizeof(uint32_t) * DMA_SIZE / 2;

	//configure clk of lcd mode, 10M
	DLH_I2S_DEVICE.sample_rate_conf.tx_bck_div_num = CONFIG_DLH_LCD_BCLK_DIV;
	DLH_I2S_DEVICE.sample_rate_conf.rx_bck_div_num = CONFIG_DLH_LCD_BCLK_DIV;
	// Change clk by modifying the value of clkm_div_num(4->10M, 8->5M ...)
	// but do not be less than 4.
	DLH_I2S_DEVICE.clkm_conf.clkm_div_b = 1;
	DLH_I2S_DEVICE.clkm_conf.clkm_div_a = 1;
	DLH_I2S_DEVICE.clkm_conf.clkm_div_num = CONFIG_DLH_LCD_CLK_DIV;
	DLH_I2S_DEVICE.clkm_conf.clk_en = 1;

	DLH_I2S_DEVICE.int_ena.out_eof = 1;
	DLH_I2S_DEVICE.int_ena.out_dscr_err = 1;

	esp_intr_enable(p_i2s_obj[CONFIG_DLH_LCD_I2S_DEVICE_NUM]->i2s_isr_handle);
	dma->queue = xQueueCreate(CONFIG_DLH_DMA_NUM_BUFS, sizeof(char *));
	dma->mux = xSemaphoreCreateMutex();
	xSemaphoreGive(dma->mux);
	return ESP_OK;

_err:
	if (dma->buf) {
		free(dma->buf);
	}
	if (*(dma->desc)) {
		free(*(dma->desc));
	}
	if (dma->desc) {
		free(dma->desc);
	}
	if (dma) {
		free(dma);
	}
	return ESP_FAIL;
}

static void IRAM_ATTR i2s_intr_handler_default(void *arg)
{
    i2s_obj_t *p_i2s = (i2s_obj_t *) arg;

    int dummy;
    portBASE_TYPE high_priority_task_awoken = 0;
    lldesc_t *finish_desc;
    if (DLH_I2S_DEVICE.int_st.out_dscr_err || DLH_I2S_DEVICE.int_st.in_dscr_err)
		{
        ESP_EARLY_LOGE(I2S_TAG, "dma error, interrupt status: 0x%08x", DLH_I2S_DEVICE.int_st.val);
    }
    if (DLH_I2S_DEVICE.int_st.out_eof && p_i2s->tx)
		{
        finish_desc = (lldesc_t *) DLH_I2S_DEVICE.out_eof_des_addr;
        // All buffers are empty. This means we have an underflow on our hands.
        if (xQueueIsQueueFullFromISR(p_i2s->tx->queue))
				{
            xQueueReceiveFromISR(p_i2s->tx->queue, &dummy, &high_priority_task_awoken);
        }
        xQueueSendFromISR(p_i2s->tx->queue, (void *)(&finish_desc->buf), &high_priority_task_awoken);
    }
    if (high_priority_task_awoken == pdTRUE) {
        portYIELD_FROM_ISR();
    }
    DLH_I2S_DEVICE.int_clr.val = DLH_I2S_DEVICE.int_st.val;
}

static esp_err_t i2s_lcd_config(void)
{
	periph_module_enable(DLH_I2S_PERIPH_I2S_MODULE);

	
	//Original init for being able to send data with the data cmd.
	DLH_I2S_DEVICE.conf.rx_fifo_reset = 1;
	DLH_I2S_DEVICE.conf.rx_fifo_reset = 0;
	DLH_I2S_DEVICE.conf.tx_fifo_reset = 1;
	DLH_I2S_DEVICE.conf.tx_fifo_reset = 0;
	DLH_I2S_DEVICE.conf.tx_reset = 1;
	DLH_I2S_DEVICE.conf.tx_reset = 0;
	DLH_I2S_DEVICE.conf.rx_reset = 1;
	DLH_I2S_DEVICE.conf.rx_reset = 0;
	DLH_I2S_DEVICE.lc_conf.in_rst = 1;
	DLH_I2S_DEVICE.lc_conf.in_rst = 0;
	DLH_I2S_DEVICE.lc_conf.out_rst = 1;
	DLH_I2S_DEVICE.lc_conf.out_rst = 0;
	DLH_I2S_DEVICE.lc_conf.check_owner = 0;
	DLH_I2S_DEVICE.lc_conf.out_loop_test = 0;
	DLH_I2S_DEVICE.lc_conf.out_auto_wrback = 0;
	DLH_I2S_DEVICE.lc_conf.out_data_burst_en = 1;
	DLH_I2S_DEVICE.lc_conf.out_no_restart_clr = 0;
	DLH_I2S_DEVICE.lc_conf.indscr_burst_en = 0;
	DLH_I2S_DEVICE.lc_conf.out_eof_mode = 1;
	DLH_I2S_DEVICE.pdm_conf.pcm2pdm_conv_en = 0;
	DLH_I2S_DEVICE.pdm_conf.pdm2pcm_conv_en = 0;
	DLH_I2S_DEVICE.fifo_conf.dscr_en = 0;
	DLH_I2S_DEVICE.conf.tx_start = 0;
	DLH_I2S_DEVICE.conf.rx_start = 0;
	DLH_I2S_DEVICE.conf.tx_slave_mod = 0;
	DLH_I2S_DEVICE.conf.tx_right_first = 1;
	DLH_I2S_DEVICE.conf1.tx_stop_en = 1;
	DLH_I2S_DEVICE.conf1.tx_pcm_bypass = 1;
	DLH_I2S_DEVICE.conf2.lcd_en = 1;
	DLH_I2S_DEVICE.conf_chan.tx_chan_mod = 2;
	DLH_I2S_DEVICE.fifo_conf.tx_fifo_mod = 0;
	DLH_I2S_DEVICE.fifo_conf.tx_fifo_mod_force_en = 1;
	DLH_I2S_DEVICE.sample_rate_conf.tx_bits_mod = 32;
	DLH_I2S_DEVICE.lc_conf.outdscr_burst_en = 1;
	DLH_I2S_DEVICE.conf.tx_msb_right = 1;

//Some usefull informations:
//https://git.koehlerweb.org/frodovdr/Sonoff-Tasmota/commit/a2e250f00881d36cc09e591f73b14b4c08bcbf27
// 	DLH_I2S_DEVICE.conf.rx_fifo_reset = 1;
// 	DLH_I2S_DEVICE.conf.rx_fifo_reset = 0;
// 	DLH_I2S_DEVICE.conf.tx_fifo_reset = 1;
// 	DLH_I2S_DEVICE.conf.tx_fifo_reset = 0;
// 	//reset i2s
// 	DLH_I2S_DEVICE.conf.tx_reset = 1;
// 	DLH_I2S_DEVICE.conf.tx_reset = 0;
// 	DLH_I2S_DEVICE.conf.rx_reset = 1;
// 	DLH_I2S_DEVICE.conf.rx_reset = 0;
// 	//reset dma
// 	DLH_I2S_DEVICE.lc_conf.in_rst = 1;
// 	DLH_I2S_DEVICE.lc_conf.in_rst = 0;
// 	DLH_I2S_DEVICE.lc_conf.out_rst = 1;
// 	DLH_I2S_DEVICE.lc_conf.out_rst = 0;
// 
// 	//Enable and configure DMA
// 	DLH_I2S_DEVICE.lc_conf.check_owner = 0;
// 	DLH_I2S_DEVICE.lc_conf.out_loop_test = 0;
// 	DLH_I2S_DEVICE.lc_conf.out_auto_wrback = 0;
// 	DLH_I2S_DEVICE.lc_conf.out_data_burst_en = 1;
// 	DLH_I2S_DEVICE.lc_conf.out_no_restart_clr = 0;
// 	DLH_I2S_DEVICE.lc_conf.indscr_burst_en = 0;
// 	DLH_I2S_DEVICE.lc_conf.out_eof_mode = 1;
// 
// 	DLH_I2S_DEVICE.pdm_conf.pcm2pdm_conv_en = 0;
// 	DLH_I2S_DEVICE.pdm_conf.pdm2pcm_conv_en = 0;
// 
// 	DLH_I2S_DEVICE.fifo_conf.dscr_en = 0;
// 
// 	DLH_I2S_DEVICE.conf.tx_start = 0;
// 	DLH_I2S_DEVICE.conf.rx_start = 0;
// 
// 	DLH_I2S_DEVICE.conf.tx_slave_mod = 0;
// 	DLH_I2S_DEVICE.conf.tx_right_first = 1;
// 	DLH_I2S_DEVICE.conf1.tx_stop_en = 1;
// 	DLH_I2S_DEVICE.conf1.tx_pcm_bypass = 1;
// 	DLH_I2S_DEVICE.conf2.lcd_en = 1;
// // 	DLH_I2S_DEVICE.conf2.lcd_tx_sdx2_en = 1;
// 	DLH_I2S_DEVICE.conf_chan.tx_chan_mod = 1;
// 	DLH_I2S_DEVICE.fifo_conf.tx_fifo_mod = 1;
// 	DLH_I2S_DEVICE.fifo_conf.tx_fifo_mod_force_en = 1;
// 	DLH_I2S_DEVICE.sample_rate_conf.tx_bits_mod = 32;
// 	DLH_I2S_DEVICE.lc_conf.outdscr_burst_en = 1;
// 	DLH_I2S_DEVICE.conf.tx_msb_right = 0;
	DLH_I2S_DEVICE.timing.tx_ws_out_delay = CONFIG_DLH_LCD_WS_TX_DATA_OUT_DELAY;
	DLH_I2S_DEVICE.timing.tx_ws_in_delay =  CONFIG_DLH_LCD_WS_TX_DATA_IN_DELAY;
	DLH_I2S_DEVICE.timing.data_enable_delay = CONFIG_DLH_LCD_TX_DATA_DELAY;
// 		DLH_I2S_DEVICE.timing.tx_ws_out_delay = 1;
// 		DLH_I2S_DEVICE.timing.tx_ws_in_delay = 1;
// 		DLH_I2S_DEVICE.timing.data_enable_delay = 0;
	return ESP_OK;
}

static esp_err_t i2s_lcd_driver_install(void)
{
	esp_err_t err;
	

	if (p_i2s_obj[CONFIG_DLH_LCD_I2S_DEVICE_NUM] == NULL) {
		p_i2s_obj[CONFIG_DLH_LCD_I2S_DEVICE_NUM] = (i2s_obj_t *) malloc(sizeof(i2s_obj_t));
		if (p_i2s_obj[CONFIG_DLH_LCD_I2S_DEVICE_NUM] == NULL) {
			ESP_LOGE(I2S_TAG, "Malloc I2S driver error");
			return ESP_ERR_NO_MEM;
		}
		memset(p_i2s_obj[CONFIG_DLH_LCD_I2S_DEVICE_NUM], 0, sizeof(i2s_obj_t));
		//To make sure hardware is enabled before any hardware register operations.

			periph_module_enable(DLH_I2S_PERIPH_I2S_MODULE);

		//initial interrupt
		err = i2s_isr_register(0, i2s_intr_handler_default, p_i2s_obj[CONFIG_DLH_LCD_I2S_DEVICE_NUM], &p_i2s_obj[CONFIG_DLH_LCD_I2S_DEVICE_NUM]->i2s_isr_handle);
		if (err != ESP_OK) {
			free(p_i2s_obj[CONFIG_DLH_LCD_I2S_DEVICE_NUM]);
			p_i2s_obj[CONFIG_DLH_LCD_I2S_DEVICE_NUM] = NULL;
			ESP_LOGE(I2S_TAG, "Register I2S Interrupt error");
			return err;
		}
		DLH_I2S_DEVICE.int_ena.val = 0;
		DLH_I2S_DEVICE.out_link.stop = 1;
		DLH_I2S_DEVICE.conf.tx_start = 0;
		err = i2s_lcd_config();
		if (err != ESP_OK) {
			i2s_driver_uninstall(CONFIG_DLH_LCD_I2S_DEVICE_NUM);
			ESP_LOGE(I2S_TAG, "I2S param configure error");
			return err;
		}
		//configure camera or lcd mode.
		err = i2s_set_parallel_mode(CONFIG_DLH_LCD_I2S_DEVICE_NUM);
		if (err != ESP_OK) {
			i2s_driver_uninstall(CONFIG_DLH_LCD_I2S_DEVICE_NUM);
			ESP_LOGE(I2S_TAG, "I2S param configure error");
			return err;
		}
		return ESP_OK;
	}
	ESP_LOGW(I2S_TAG, "I2S driver already installed");
	return ESP_OK;
}

bool i2s_lcd_create(void)
{
// 	i2s_lcd_t* i2s_lcd = (i2s_lcd_t*) calloc(1, sizeof(i2s_lcd_t));
// 	i2s_lcd->i2s_lcd_conf = *pin_conf;
// 	printf("i2s rs: %d, now: %d", pin_conf->rs_io_num, i2s_lcd->i2s_lcd_conf.rs_io_num);
	ESP_LOGI(I2S_TAG, "i2s_lcd_create I2S NUM:%d", CONFIG_DLH_LCD_I2S_DEVICE_NUM);
	esp_err_t ret = i2s_lcd_driver_install();
	if(ret != ESP_OK)
	{
		ESP_LOGE(I2S_TAG, "Error while installing i2s driver");
		goto error;
	}
	gpio_config_t gpio_conf = {0};


	gpio_conf.pin_bit_mask = DLH_I2S_PIN_BITMASK;
	gpio_conf.mode =  GPIO_MODE_OUTPUT;
	gpio_conf.pull_up_en = 0;
	gpio_conf.intr_type = GPIO_INTR_DISABLE;
	if (gpio_config(&gpio_conf) != ESP_OK)
	{
		ESP_LOGE(I2S_TAG, "Error while setting pin config for i2s");
		return false;
	}

	for (int i = 0; i < DLH_LCD_BUS_WIDTH; i++) {
			gpio_matrix_out(dlh_lcd_bus[i], DLH_I2S_O_DATA_OUT_IDX + i, false, false);
	}

	gpio_matrix_out(CONFIG_DLH_LCD_PIN_WR, DLH_I2S_O_WS_OUT_IDX, true, false);

	ESP_LOGI(I2S_TAG, "i2s_lcd_create with: %d done", CONFIG_DLH_LCD_I2S_DEVICE_NUM);
	return true;

	error:
	return false;
}

#ifdef  CONFIG_DLH_LCD_DISPLAY_BUS_DRIVER_8BIT

int i2s_lcd_write_data(const char *src, size_t size, TickType_t ticks_to_wait, bool swap)
{
	uint8_t tagle = 0x01;
	uint8_t *ptr = (uint8_t *)src;
	size_t size_remain = size;
	size_t write_cnt = 0;
	lldesc_t *desc = *(p_i2s_obj[CONFIG_DLH_LCD_I2S_DEVICE_NUM]->tx->desc);
	uint32_t *buf = NULL;
	uint32_t loop_cnt = 0;
	xSemaphoreTake(p_i2s_obj[CONFIG_DLH_LCD_I2S_DEVICE_NUM]->tx->mux, (portTickType)portMAX_DELAY);
	buf = (uint32_t *)(p_i2s_obj[CONFIG_DLH_LCD_I2S_DEVICE_NUM]->tx->buf[tagle ^ 0x1]);
	write_cnt = size_remain > p_i2s_obj[CONFIG_DLH_LCD_I2S_DEVICE_NUM]->tx->buf_size ? p_i2s_obj[CONFIG_DLH_LCD_I2S_DEVICE_NUM]->tx->buf_size : size_remain;
	for (loop_cnt = 0; loop_cnt < write_cnt; loop_cnt += 2) {
		if (swap) {
			buf[loop_cnt] = ptr[loop_cnt+1];
			buf[loop_cnt+1] = ptr[loop_cnt];
		} else {
			buf[loop_cnt] = ptr[loop_cnt];
			buf[loop_cnt+1] = ptr[loop_cnt+1];
		}
	}
	ptr += write_cnt;
	size_remain -= write_cnt;
	while (1) {
		desc->buf = (uint8_t *)(p_i2s_obj[CONFIG_DLH_LCD_I2S_DEVICE_NUM]->tx->buf[tagle ^ 0x1]);
		desc->length = write_cnt * sizeof(uint32_t);
		desc->size = write_cnt * sizeof(uint32_t);
		DLH_I2S_DEVICE.out_link.addr = ((uint32_t)(desc))&I2S_OUTLINK_ADDR;
		DLH_I2S_DEVICE.out_link.start = 1;
		DLH_I2S_DEVICE.fifo_conf.dscr_en = 1;
		DLH_I2S_DEVICE.conf.tx_start = 1;
		tagle ^= 0x1;
		buf = (uint32_t *)(p_i2s_obj[CONFIG_DLH_LCD_I2S_DEVICE_NUM]->tx->buf[tagle ^ 0x1]);
		write_cnt = size_remain > p_i2s_obj[CONFIG_DLH_LCD_I2S_DEVICE_NUM]->tx->buf_size ? p_i2s_obj[CONFIG_DLH_LCD_I2S_DEVICE_NUM]->tx->buf_size : size_remain;
		for (loop_cnt = 0; loop_cnt < write_cnt; loop_cnt += 2) {
				if (swap) {
						buf[loop_cnt] = ptr[loop_cnt+1];
						buf[loop_cnt+1] = ptr[loop_cnt];
				} else {
						buf[loop_cnt] = ptr[loop_cnt];
						buf[loop_cnt+1] = ptr[loop_cnt+1];
				}
		}
		ptr += write_cnt;
		if (xQueueReceive(p_i2s_obj[CONFIG_DLH_LCD_I2S_DEVICE_NUM]->tx->queue, &p_i2s_obj[CONFIG_DLH_LCD_I2S_DEVICE_NUM]->tx->curr_ptr, ticks_to_wait) == pdFALSE) {
				break;
		}
		DLH_I2S_DEVICE.conf.tx_start = 0;
		DLH_I2S_DEVICE.conf.tx_reset = 1;
		DLH_I2S_DEVICE.conf.tx_reset = 0;
		if (size_remain == 0) {
				break;
		}
		size_remain -= write_cnt;
	}
	xSemaphoreGive(p_i2s_obj[CONFIG_DLH_LCD_I2S_DEVICE_NUM]->tx->mux);
	DLH_I2S_DEVICE.fifo_conf.dscr_en = 0;
	return size - size_remain;
}

#else //CONFIG_BIT_MODE_16BIT



#define DMA_CHUNK_WORD_SIZE 2000

int i2s_lcd_write_data(const char *src, size_t size, TickType_t ticks_to_wait, bool swap)
{
// 	uint8_t tagle = 0x01;
	uint16_t *ptr = (uint16_t *)src;
	size_t size_remain = size / 2;
	printf("Want to print: %d pixel\n", size_remain);
// 	uint32_t current_chunk;
// 	size_t write_cnt = 0;
	lldesc_t *desc = *(p_i2s_obj[CONFIG_DLH_LCD_I2S_DEVICE_NUM]->tx->desc);
// 	uint32_t *buf = NULL;
// 	uint32_t loop_cnt = 0;
	xSemaphoreTake(p_i2s_obj[CONFIG_DLH_LCD_I2S_DEVICE_NUM]->tx->mux, (portTickType)portMAX_DELAY);
// 	buf = (uint32_t *)(p_i2s_obj[CONFIG_DLH_LCD_I2S_DEVICE_NUM]->tx->buf[tagle ^ 0x1]);
// 	write_cnt = size_remain > p_i2s_obj[CONFIG_DLH_LCD_I2S_DEVICE_NUM]->tx->buf_size ? p_i2s_obj[CONFIG_DLH_LCD_I2S_DEVICE_NUM]->tx->buf_size : size_remain;
// 	for (loop_cnt = 0; loop_cnt < write_cnt; loop_cnt++)
// 	{
// 		buf[loop_cnt] = ptr[loop_cnt];
// 	}
// 	ptr += write_cnt;
// 	size_remain -= write_cnt;
	while (1)
	{
		desc->buf = (uint8_t *)ptr;
		if(size_remain < DMA_CHUNK_WORD_SIZE)
		{
			desc->length = size_remain * sizeof(uint16_t);
			desc->size = size_remain * sizeof(uint16_t);
			size_remain = 0;
		}
		else
		{
			desc->length = DMA_CHUNK_WORD_SIZE * sizeof(uint16_t);
			desc->size = DMA_CHUNK_WORD_SIZE * sizeof(uint16_t);
			size_remain -= DMA_CHUNK_WORD_SIZE;
			ptr += DMA_CHUNK_WORD_SIZE;
		}
		DLH_I2S_DEVICE.out_link.addr = ((uint32_t)(desc))&I2S_OUTLINK_ADDR;
		DLH_I2S_DEVICE.out_link.start = 1;
		DLH_I2S_DEVICE.fifo_conf.dscr_en = 1;
		DLH_I2S_DEVICE.conf.tx_start = 1;
// 		tagle ^= 0x1;

// 		write_cnt = size_remain > p_i2s_obj[CONFIG_DLH_LCD_I2S_DEVICE_NUM]->tx->buf_size ? p_i2s_obj[CONFIG_DLH_LCD_I2S_DEVICE_NUM]->tx->buf_size : size_remain;
// 		for (loop_cnt = 0; loop_cnt < write_cnt; loop_cnt++) {
// 				buf[loop_cnt] = ptr[loop_cnt];
// 		}
// 		ptr += write_cnt;
		if (xQueueReceive(p_i2s_obj[CONFIG_DLH_LCD_I2S_DEVICE_NUM]->tx->queue, &p_i2s_obj[CONFIG_DLH_LCD_I2S_DEVICE_NUM]->tx->curr_ptr, ticks_to_wait) == pdFALSE) {
				break;
		}
		DLH_I2S_DEVICE.conf.tx_start = 0;
		DLH_I2S_DEVICE.conf.tx_reset = 1;
		DLH_I2S_DEVICE.conf.tx_reset = 0;
		if (size_remain == 0) {
				break;
		}
	}
	xSemaphoreGive(p_i2s_obj[CONFIG_DLH_LCD_I2S_DEVICE_NUM]->tx->mux);
	DLH_I2S_DEVICE.fifo_conf.dscr_en = 0;
	return size - (size_remain * 2);
}



int i2s_lcd_write_data2(const char *src, size_t size, TickType_t ticks_to_wait, bool swap)
{

	uint8_t tagle = 0x01;
	uint16_t *ptr = (uint16_t *)src;
	size_t size_remain = size / 2;
	size_t write_cnt = 0;
	lldesc_t *desc = *(p_i2s_obj[CONFIG_DLH_LCD_I2S_DEVICE_NUM]->tx->desc);
	uint32_t *buf = NULL;
	uint32_t loop_cnt = 0;
	xSemaphoreTake(p_i2s_obj[CONFIG_DLH_LCD_I2S_DEVICE_NUM]->tx->mux, (portTickType)portMAX_DELAY);
	buf = (uint32_t *)(p_i2s_obj[CONFIG_DLH_LCD_I2S_DEVICE_NUM]->tx->buf[tagle ^ 0x1]);
	write_cnt = size_remain > p_i2s_obj[CONFIG_DLH_LCD_I2S_DEVICE_NUM]->tx->buf_size ? p_i2s_obj[CONFIG_DLH_LCD_I2S_DEVICE_NUM]->tx->buf_size : size_remain;
	for (loop_cnt = 0; loop_cnt < write_cnt; loop_cnt++)
	{
		buf[loop_cnt] = ptr[loop_cnt];
	}
	ptr += write_cnt;
	size_remain -= write_cnt;
	while (1)
	{
		desc->buf = (uint8_t *)(p_i2s_obj[CONFIG_DLH_LCD_I2S_DEVICE_NUM]->tx->buf[tagle ^ 0x1]);
		desc->length = write_cnt * sizeof(uint32_t);
		desc->size = write_cnt * sizeof(uint32_t);
		DLH_I2S_DEVICE.out_link.addr = ((uint32_t)(desc))&I2S_OUTLINK_ADDR;
		DLH_I2S_DEVICE.out_link.start = 1;
		DLH_I2S_DEVICE.fifo_conf.dscr_en = 1;
		DLH_I2S_DEVICE.conf.tx_start = 1;
		tagle ^= 0x1;
		buf = (uint32_t *)(p_i2s_obj[CONFIG_DLH_LCD_I2S_DEVICE_NUM]->tx->buf[tagle ^ 0x1]);
		write_cnt = size_remain > p_i2s_obj[CONFIG_DLH_LCD_I2S_DEVICE_NUM]->tx->buf_size ? p_i2s_obj[CONFIG_DLH_LCD_I2S_DEVICE_NUM]->tx->buf_size : size_remain;
		for (loop_cnt = 0; loop_cnt < write_cnt; loop_cnt++) {
				buf[loop_cnt] = ptr[loop_cnt];
		}
		ptr += write_cnt;
		if (xQueueReceive(p_i2s_obj[CONFIG_DLH_LCD_I2S_DEVICE_NUM]->tx->queue, &p_i2s_obj[CONFIG_DLH_LCD_I2S_DEVICE_NUM]->tx->curr_ptr, ticks_to_wait) == pdFALSE) {
				break;
		}
		DLH_I2S_DEVICE.conf.tx_start = 0;
		DLH_I2S_DEVICE.conf.tx_reset = 1;
		DLH_I2S_DEVICE.conf.tx_reset = 0;
		if (size_remain == 0) {
				break;
		}
		size_remain -= write_cnt;
	}
	xSemaphoreGive(p_i2s_obj[CONFIG_DLH_LCD_I2S_DEVICE_NUM]->tx->mux);
	DLH_I2S_DEVICE.fifo_conf.dscr_en = 0;
	return size - (size_remain * 2);
}

#endif // CONFIG_BIT_MODE_16BIT
