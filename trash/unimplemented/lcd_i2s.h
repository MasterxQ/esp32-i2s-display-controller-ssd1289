#ifndef DLH_LCD_I2S_H__
#define DLH_LCD_I2S_H__ 1
#include <stdint.h>
#include <stdbool.h>
#include <sdkconfig.h>
#include "freertos/FreeRTOS.h"


#ifdef CONFIG_DLH_LCD_DISPLAY_DRIVER_SSD1289
#include "driver/lcd_i2s_ssd1289.h"
#include "driver/lcd_i2s_ssd1289_defines.h"
#endif

#ifdef __cplusplus
extern "C"
{
#endif

extern bool lcd_init(void);
extern void lcd_refresh(void);
extern void lcd_fill_screen(uint16_t color);
void lcd_set_box(uint16_t x, uint16_t y, uint16_t x_size, uint16_t y_size);
void lcd_draw_section (void *src, uint16_t x, uint16_t y, uint16_t width, uint16_t height);


#ifdef __cplusplus
}
#endif

#endif
