// Copyright 2015-2016 Espressif Systems (Shanghai) PTE LTD
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#include <stdio.h>
#include "esp_system.h"
#include "soc/soc.h"
#include "soc/dport_reg.h"
#include "driver/gpio.h"
#include <lcd_i2s_com.h>
#include "include/iot_i2s_lcd.h"
#include "sdkconfig.h"
#include <esp32/rom/ets_sys.h>

#include "lcd_i2s_helper.h"

#if CONFIG_DLH_LCD_PIN_RS < 32
    #define CLR_RS() GPIO.out_w1tc = (1 << (CONFIG_DLH_LCD_PIN_RS))
    #define SET_RS() GPIO.out_w1ts = (1 << (CONFIG_DLH_LCD_PIN_RS))
#else
    #define CLR_RS() GPIO.out1_w1tc.data = (1 << (CONFIG_DLH_LCD_PIN_RS-32))
    #define SET_RS() GPIO.out1_w1ts.data = (1 << (CONFIG_DLH_LCD_PIN_RS-32))
#endif

#define WAIT_FOR_WORK_TIMEOUT 20

// #define SWAP_UNSIGNED16(var) var = (var << 8)| ((var >> 8) & 0xFF);
#define SWAP_UNSIGNED16(var) (void)var

// static i2s_dev_t *I2S[I2S_NUM_MAX] = {&I2S0, &I2S1};
// static uint32_t I2S_FIFO_ADD[I2S_NUM_MAX] = {I2S0_FIFO_ADD, I2S1_FIFO_ADD};

#if defined(CONFIG_DLH_LCD_DISPLAY_BUS_DRIVER_8BIT)

void iot_i2s_lcd_write_data(uint16_t data)
{
    iot_i2s_lcd_write_data8(data >> 8);
    iot_i2s_lcd_write_data8((uint8_t)data);
}
void iot_i2s_lcd_write_data8(uint8_t data)
{
    i2s_lcd_write_data((char *)&data, 2, 100, true);
}

void iot_i2s_lcd_write_cmd8(uint8_t cmd)
{
    CLR_RS();
    REG_WRITE(DLH_I2S_FIFO_ADDR, cmd >> 8);
    DLH_I2S_DEVICE.conf.tx_start = 1;
    while (!(DLH_I2S_DEVICE.state.tx_idle)) {
        // vTaskDelay(20);
        ;
    }
    DLH_I2S_DEVICE.conf.tx_start = 0;
    DLH_I2S_DEVICE.conf.tx_reset = 1;
    DLH_I2S_DEVICE.conf.tx_reset = 0;
    DLH_I2S_DEVICE.conf.tx_fifo_reset = 1;
    DLH_I2S_DEVICE.conf.tx_fifo_reset = 0;
    SET_RS();
}

void iot_i2s_lcd_write_reg(uint16_t cmd, uint16_t data)
{
    CLR_RS();
    i2s_lcd_write_data((char *)&cmd, 2, 100, true);
    SET_RS();
    i2s_lcd_write_data((char *)&data, 2, 100, true);
}

void iot_i2s_lcd_write(uint16_t *data, uint32_t len)
{
    i2s_lcd_write_data((char *)data, len, 100, true);
}

#elif defined(CONFIG_DLH_LCD_DISPLAY_BUS_DRIVER_16BIT) // CONFIG_BIT_MODE_16BIT

void iot_i2s_lcd_write_cmd_data(uint16_t cmd, uint16_t data)
{
	iot_i2s_lcd_write_cmd(cmd);
	iot_i2s_lcd_write_data(data);
}

void iot_i2s_lcd_write_data(uint16_t data)
{
	SWAP_UNSIGNED16(data);
// 	printf("writing data only\n");

	REG_WRITE(DLH_I2S_FIFO_ADDR, data);
	DLH_I2S_DEVICE.conf.tx_start = 1;

	for(uint32_t i = 0; i < WAIT_FOR_WORK_TIMEOUT; i++)
	{
		if(!DLH_I2S_DEVICE.state.tx_idle)
		{
// 			printf("got not idle:%d\n", i);
			break;
		}
	}

	while (!DLH_I2S_DEVICE.state.tx_idle)
	{
			// vTaskDelay(20);
	}
	DLH_I2S_DEVICE.conf.tx_start = 0;
	DLH_I2S_DEVICE.conf.tx_reset = 1;
	DLH_I2S_DEVICE.conf.tx_reset = 0;
	DLH_I2S_DEVICE.conf.tx_fifo_reset = 1;
	DLH_I2S_DEVICE.conf.tx_fifo_reset = 0;
}

void iot_i2s_lcd_write_cmd(uint16_t cmd)
{
	SWAP_UNSIGNED16(cmd);
// 	printf("writing cmd only\n");
    CLR_RS();
	REG_WRITE(DLH_I2S_FIFO_ADDR, cmd);
	DLH_I2S_DEVICE.conf.tx_start = 1;
	
	for(uint32_t i = 0; i < WAIT_FOR_WORK_TIMEOUT; i++) //For div 64 @ 240 MHz up to 7, so 20 maybe to high timeout, it should scale with div clk. If no other flag can be read, somebody should calc the max delay and put it in asm?
	{
		if(!DLH_I2S_DEVICE.state.tx_idle)
		{
// 			printf("got not idle:%d\n", i);
			break;
		}
	}

	while (!(DLH_I2S_DEVICE.state.tx_idle)) {
			// vTaskDelay(20);
			;
	}
	DLH_I2S_DEVICE.conf.tx_start = 0;
	DLH_I2S_DEVICE.conf.tx_reset = 1;
	DLH_I2S_DEVICE.conf.tx_reset = 0;
	DLH_I2S_DEVICE.conf.tx_fifo_reset = 1;
	DLH_I2S_DEVICE.conf.tx_fifo_reset = 0;
    SET_RS();
}

void iot_i2s_lcd_write_reg(uint16_t cmd, uint16_t data)
{
	SWAP_UNSIGNED16(data);
	SWAP_UNSIGNED16(cmd);
// 	printf("cmd: %x, data: %x\n", cmd, data);
// 	i2s_lcd_t *i2s_lcd = (i2s_lcd_t *)i2s_lcd_handle;
#define i2s_num CONFIG_DLH_LCD_I2S_DEVICE_NUM
	GPIO.out_w1tc = (1 << CONFIG_DLH_LCD_PIN_RS);
	REG_WRITE(DLH_I2S_FIFO_ADDR, cmd);

	DLH_I2S_DEVICE.conf.tx_start = 1;
	for(uint32_t i = 0; i < WAIT_FOR_WORK_TIMEOUT; i++)
	{
		if(!DLH_I2S_DEVICE.state.tx_idle)
		{
// 			printf("got not idle:%d\n", i);
			break;
		}
	}

	while (!(DLH_I2S_DEVICE.state.tx_idle))
	{
// 			printf("Waiting for idle cmd\n");
	}
// 	ets_delay_us(100);
	DLH_I2S_DEVICE.conf.tx_start = 0;
	DLH_I2S_DEVICE.conf.tx_reset = 1;
	DLH_I2S_DEVICE.conf.tx_reset = 0;
	DLH_I2S_DEVICE.conf.tx_fifo_reset = 1;
	DLH_I2S_DEVICE.conf.tx_fifo_reset = 0;
    SET_RS();

	REG_WRITE(DLH_I2S_FIFO_ADDR, data);
	DLH_I2S_DEVICE.conf.tx_start = 1;

	for(uint32_t i = 0; i < WAIT_FOR_WORK_TIMEOUT; i++)
	{
		if(!DLH_I2S_DEVICE.state.tx_idle)
		{
// 			printf("got not idle:%d\n", i);
			break;
		}
	}

	while (!(DLH_I2S_DEVICE.state.tx_idle))
	{
		
	}
// 	ets_delay_us(100);
	DLH_I2S_DEVICE.conf.tx_start = 0;
	DLH_I2S_DEVICE.conf.tx_reset = 1;
	DLH_I2S_DEVICE.conf.tx_reset = 0;
	DLH_I2S_DEVICE.conf.tx_fifo_reset = 1;
	DLH_I2S_DEVICE.conf.tx_fifo_reset = 0;
}

void iot_i2s_lcd_write(uint16_t *data, uint32_t len)
{
    i2s_lcd_write_data((char *)data, len, 100, false);
}

#else // CONFIG_BIT_MODE_16BIT
#error No bus bandwidth selected
#endif

bool iot_i2s_lcd_pin_cfg(void)
{
	printf("init i2s\n");
	if(!i2s_lcd_create())
	{
		printf("init i2s failed\n");
		return false;
	}
	printf("init i2s done\n");

	//This pin is not part of the I2S interface.
	gpio_set_direction(CONFIG_DLH_LCD_PIN_RS, GPIO_MODE_OUTPUT);
	gpio_set_pull_mode(CONFIG_DLH_LCD_PIN_RS, GPIO_PULLUP_ONLY);
	PIN_FUNC_SELECT(GPIO_PIN_MUX_REG[CONFIG_DLH_LCD_PIN_RS], PIN_FUNC_GPIO);

	//TODO move drive cap to config
	gpio_set_drive_capability(CONFIG_DLH_LCD_PIN_DB0, GPIO_DRIVE_CAP_0);
	gpio_set_drive_capability(CONFIG_DLH_LCD_PIN_DB1, GPIO_DRIVE_CAP_0);
	gpio_set_drive_capability(CONFIG_DLH_LCD_PIN_DB2, GPIO_DRIVE_CAP_0);
	gpio_set_drive_capability(CONFIG_DLH_LCD_PIN_DB3, GPIO_DRIVE_CAP_0);
	gpio_set_drive_capability(CONFIG_DLH_LCD_PIN_DB4, GPIO_DRIVE_CAP_0);
	gpio_set_drive_capability(CONFIG_DLH_LCD_PIN_DB5, GPIO_DRIVE_CAP_0);
	gpio_set_drive_capability(CONFIG_DLH_LCD_PIN_DB6, GPIO_DRIVE_CAP_0);
	gpio_set_drive_capability(CONFIG_DLH_LCD_PIN_DB7, GPIO_DRIVE_CAP_0);
#ifdef CONFIG_DLH_LCD_DISPLAY_BUS_DRIVER_16BIT
	gpio_set_drive_capability(CONFIG_DLH_LCD_PIN_DB8, GPIO_DRIVE_CAP_0);
	gpio_set_drive_capability(CONFIG_DLH_LCD_PIN_DB9, GPIO_DRIVE_CAP_0);
	gpio_set_drive_capability(CONFIG_DLH_LCD_PIN_DB10, GPIO_DRIVE_CAP_0);
	gpio_set_drive_capability(CONFIG_DLH_LCD_PIN_DB11, GPIO_DRIVE_CAP_0);
	gpio_set_drive_capability(CONFIG_DLH_LCD_PIN_DB12, GPIO_DRIVE_CAP_0);
	gpio_set_drive_capability(CONFIG_DLH_LCD_PIN_DB13, GPIO_DRIVE_CAP_0);
	gpio_set_drive_capability(CONFIG_DLH_LCD_PIN_DB14, GPIO_DRIVE_CAP_0);
	gpio_set_drive_capability(CONFIG_DLH_LCD_PIN_DB15, GPIO_DRIVE_CAP_0);
#endif

	printf("rs pin set\n");
	return true;
}
