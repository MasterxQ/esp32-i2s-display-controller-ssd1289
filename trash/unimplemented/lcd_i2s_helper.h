#ifndef DLH_LCD_I2S_HELPER_H__
#define DLH_LCD_I2S_HELPER_H__ 1

#include <stdint.h>
#include <sdkconfig.h>

#if CONFIG_DLH_LCD_I2S_DEVICE_NUM == 0

#define DLH_I2S_DEVICE                I2S0
#define DLH_I2S_PERIPH_I2S_MODULE     PERIPH_I2S0_MODULE
#define DLH_I2S_O_WS_OUT_IDX            I2S0O_WS_OUT_IDX
#define DLH_I2S_O_DATA_OUT8_IDX         I2S0O_DATA_OUT8_IDX
#define I2S_FIFO_ADDR                 0x6000f000

#elif CONFIG_DLH_LCD_I2S_DEVICE_NUM == 1

#define DLH_I2S_DEVICE                I2S1
#define DLH_I2S_PERIPH_I2S_MODULE     PERIPH_I2S1_MODULE
#define DLH_I2S_O_WS_OUT_IDX        I2S1O_WS_OUT_IDX
#ifdef CONFIG_DLH_LCD_DISPLAY_BUS_DRIVER_16BIT
#define DLH_I2S_O_DATA_OUT_IDX       I2S1O_DATA_OUT8_IDX
#elif defined (CONFIG_DLH_LCD_DISPLAY_BUS_DRIVER_8BIT)
#define DLH_I2S_O_DATA_OUT_IDX       I2S1O_DATA_OUT0_IDX
#else 
#error this LCD_BUS_WIDTH not implemented
#endif
#define DLH_I2S_FIFO_ADDR                 0x6002d000

#else
#error unknown i2s device num in CONFIG_DLH_LCD_I2S_DEVICE_NUM.
#endif



#if defined(CONFIG_DLH_LCD_DISPLAY_BUS_DRIVER_16BIT)
extern const uint8_t dlh_lcd_bus[16];
#define DLH_I2S_DATA_PIN_BITMASK (((uint64_t)1 << CONFIG_DLH_LCD_PIN_DB0) |          \
																	((uint64_t)1 << CONFIG_DLH_LCD_PIN_DB1) |          \
																	((uint64_t)1 << CONFIG_DLH_LCD_PIN_DB2) |          \
																	((uint64_t)1 << CONFIG_DLH_LCD_PIN_DB3) |          \
																	((uint64_t)1 << CONFIG_DLH_LCD_PIN_DB4) |          \
																	((uint64_t)1 << CONFIG_DLH_LCD_PIN_DB5) |          \
																	((uint64_t)1 << CONFIG_DLH_LCD_PIN_DB6) |          \
																	((uint64_t)1 << CONFIG_DLH_LCD_PIN_DB7) |          \
																	((uint64_t)1 << CONFIG_DLH_LCD_PIN_DB8) |          \
																	((uint64_t)1 << CONFIG_DLH_LCD_PIN_DB9) |          \
																	((uint64_t)1 << CONFIG_DLH_LCD_PIN_DB10) |         \
																	((uint64_t)1 << CONFIG_DLH_LCD_PIN_DB11) |         \
																	((uint64_t)1 << CONFIG_DLH_LCD_PIN_DB12) |         \
																	((uint64_t)1 << CONFIG_DLH_LCD_PIN_DB13) |         \
																	((uint64_t)1 << CONFIG_DLH_LCD_PIN_DB14) |         \
																	((uint64_t)1 << CONFIG_DLH_LCD_PIN_DB15))
#define DLH_LCD_BUS_WIDTH 16
#elif defined(CONFIG_DLH_LCD_DISPLAY_BUS_DRIVER_8BIT)
extern const uint8_t dlh_lcd_bus[8];
#define DLH_I2S_DATA_PIN_BITMASK (((uint64_t)1 << CONFIG_DLH_LCD_PIN_DB0) |          \
																	((uint64_t)1 << CONFIG_DLH_LCD_PIN_DB1) |          \
																	((uint64_t)1 << CONFIG_DLH_LCD_PIN_DB2) |          \
																	((uint64_t)1 << CONFIG_DLH_LCD_PIN_DB3) |          \
																	((uint64_t)1 << CONFIG_DLH_LCD_PIN_DB4) |          \
																	((uint64_t)1 << CONFIG_DLH_LCD_PIN_DB5) |          \
																	((uint64_t)1 << CONFIG_DLH_LCD_PIN_DB6) |          \
																	((uint64_t)1 << CONFIG_DLH_LCD_PIN_DB7))
#define DLH_LCD_BUS_WIDTH 8
#warning LCD interface 8bit implementation not completed.
#else
#error LCD interface unknown.
#endif





#ifdef CONFIG_DLH_LCD_USE_RESET
#define DLH_I2S_RESET_BITMASK (((uint64_t)1 << CONFIG_DLH_LCD_PIN_RESET))
#else
#define DLH_I2S_RESET_BITMASK 0
#endif



#define DLH_I2S_PIN_BITMASK ((uint64_t)1 << CONFIG_DLH_LCD_PIN_WR) | DLH_I2S_RESET_BITMASK | DLH_I2S_DATA_PIN_BITMASK







#endif
