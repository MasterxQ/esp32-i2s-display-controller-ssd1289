#include "lcd_i2s_text.h"
#include <graphics/lcd_font.h>

#include "stdio.h"

#include <lcd_i2s.h>
#include <lcd_i2s_com.h>
#include <string.h>



uint16_t print_text(const char* text, int16_t x, int16_t y, uint8_t multiplier, int16_t line_space, lcd_pixel_raw_t fg_color, lcd_pixel_raw_t bg_color, bool rotate, uint8_t font)
{
	int16_t last_ok;



	uint16_t width = font_list[font].width;

	uint16_t height = font_list[font].height;

	if(rotate)
	{
		int16_t y_orig = y;
		
		if( (int16_t) y - (int16_t) line_space <= 0 && line_space != 0)
		{
			last_ok = y - line_space + width;
		}
		else
		{
			last_ok = 0 + width - 1;
		}
		
		if( ((int16_t) y) - width < 0)
		{
			return 0;
		}
		
		while(*text)
		{
			if(*text == '\n')
			{
				y = y_orig;
				x += height;
				text++;
			}
			print_char(*text, x, y, multiplier, fg_color, bg_color, rotate, font);
			
			y -= width;
			
			text++;
			
			if(y < last_ok)
			{
				y  = y_orig;
				x += height;
			}
		}
	}
	else
	{
		
		int16_t x_orig = x;
		if(x + line_space < CONFIG_DLH_LCD_DISPLAY_SIZE_X && line_space != 0)
		{
			last_ok = x + line_space - width;
		}
		else
		{
			last_ok = (CONFIG_DLH_LCD_DISPLAY_SIZE_X - 1) - width + 1;
		}
		
		if(last_ok < x)
		{
			return 0;
		}
		
		while(*text)
		{
			if(*text == '\n')
			{
				x = x_orig;
				y += height;
				text++;
				continue;
			}
			print_char(*text, x, y, multiplier, fg_color, bg_color, rotate, font);
			
			x += width;
			
			text++;
			
			if(x > last_ok)
			{
				x = x_orig;
				y += height;
			}
		}
	}
	return 1;
	}



void print_char(char ascii, int16_t x, int16_t y, uint8_t multiplier, lcd_pixel_raw_t fg_color, lcd_pixel_raw_t bg_color, bool rotate, uint8_t font)
{
	int8_t xc = 0, bc = 0, i;

	uint8_t font_width = font_list[font].width;



	uint8_t font_height = font_list[font].height;

	uint8_t bytes_per_col = font_height >> 3;

	uint8_t bytes_per_char = bytes_per_col * font_width;



		uint8_t c[bytes_per_char];
		memcpy(c, ((const uint8_t *)font_list[font].font_data) + (bytes_per_char * (uint8_t) ascii), bytes_per_char);

// 	const uint8_t *c = fonts[font].font_data + (bytes_per_char * (uint8_t) ascii);
// 	memcpy_P(c, ((const uint8_t *)fonts[font].flash_mem_pos) + (bytes_per_char * (uint8_t) ascii), bytes_per_char);

	if(rotate)
	{
		y -= font_width - 1;
		uint8_t data;
		lcd_set_box(x, y, font_height, font_width);
		
		for(xc = bytes_per_char - 1; xc >= 0; xc -= bytes_per_col)
		{
			for(i = bytes_per_col - 1; i >= 0; i--)
			{
				data = c[xc - i];
				for(bc = 0; bc < 8; bc++)
				{
					if(data & 0b10000000)
					{
						iot_i2s_lcd_write_data(fg_color);
					}
					else
					{
						iot_i2s_lcd_write_data(bg_color);
					}
					
					data <<= 1;
				}
			}
		}
	}
	else
	{
		lcd_set_box(x, y, font_width, font_height);

		for(i = 0; i < bytes_per_col; i++)
		{
			for(bc = 0; bc < 8; bc++)
			{
				for(xc = i; xc < bytes_per_char; xc+=bytes_per_col)
				{
					if(c[xc] & 0b10000000)
					{
						iot_i2s_lcd_write_data(fg_color);
					}
					else
					{
						iot_i2s_lcd_write_data(bg_color);
					}
					
					c[xc] <<= 1;
				}
			}
		}
	}
}

struct
{
  int16_t x;
  int16_t y;
  uint8_t font;
  int16_t last_x;
  int16_t last_y;
  int16_t cur_x;
  int16_t cur_y;
  lcd_pixel_raw_t txt_color;
  lcd_pixel_raw_t bg_color;
} printf_box;

// bool display_printf_put(uint8_t byte)
// {
//   print_char(byte, printf_box.cur_x, printf_box.cur_y, 1, printf_box.txt_color, printf_box.bg_color, 1, printf_box.font);
//   printf_box.cur_y -= fonts[printf_box.font].width;
//   if(printf_box.cur_y < printf_box.last_y || byte == '\n' || byte == '\r')
//   {
//     printf_box.cur_y = printf_box.y;
//     printf_box.cur_x += fonts[printf_box.font].height;
//     if(printf_box.cur_x > printf_box.last_x)
//     {
//       printf_box.cur_x = printf_box.x;
//     }
//     if(byte != '\n' || byte != '\r')
// 			DRAW_RECT(printf_box.cur_x, printf_box.last_y - fonts[printf_box.font].width + 1, printf_box.cur_x + fonts[printf_box.font].height - 1, printf_box.y, printf_box.bg_color);
//   }
//   return true;
// }
// 
// 
// typedef int (*__display_receive_f_ptr)(char c_out, FILE* stream);
// 
// FILE _display_std_file_out = FDEV_SETUP_STREAM( (__display_receive_f_ptr) &display_printf_put, NULL, _FDEV_SETUP_WRITE );
// 
// 
// 
// void setup_printf(int16_t x, int16_t y, uint16_t width, uint16_t height, uint8_t font, lcd_pixel_raw_t text_color, lcd_pixel_raw_t bg_color, bool redirect)
// {
// 	//Text is rotated x and y is swapped additional.
// 	//You need to specify a left dot at start point.
// 	DRAW_RECT(x, y, x + width - 1, y + height - 1, bg_color);
//   printf_box.font = font;
//   printf_box.x = x;
//   printf_box.cur_x = printf_box.x;
//   printf_box.y = y + height - 1;
//   printf_box.cur_y = printf_box.y;
//   printf_box.last_x = x + width - fonts[font].height;
//   printf_box.last_y = y + fonts[font].width;
//   printf_box.bg_color = bg_color;
//   printf_box.txt_color = text_color;
// //   DRAW_RECT(x, y - width + 1, x + height - 1, y, bg_color);
//   if(redirect)
//   {
//     stdout = &_display_std_file_out;
//   }
// }
