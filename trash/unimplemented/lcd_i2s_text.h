#ifndef __LCD_TEXT_H__
#define __LCD_TEXT_H__ 1

#include <stdint.h>
#include <stdbool.h>
#include <lcd_i2s_types.h>

#ifdef __cplusplus
extern "C"
{
#endif

void print_char(char ascii, int16_t x, int16_t y, uint8_t multiplier, lcd_pixel_raw_t fg_color, lcd_pixel_raw_t bg_color, bool rotate, uint8_t font);

uint16_t print_text(const char* text, int16_t x, int16_t y, uint8_t multiplier, int16_t line_space, lcd_pixel_raw_t fg_color, lcd_pixel_raw_t bg_color, bool rotate, uint8_t font);

void setup_printf(int16_t x, int16_t y, uint16_t width, uint16_t height, uint8_t font, lcd_pixel_raw_t text_color, lcd_pixel_raw_t bg_color, bool redirect);

bool display_printf_put(uint8_t byte);


#ifdef __cplusplus
}
#endif

#endif

