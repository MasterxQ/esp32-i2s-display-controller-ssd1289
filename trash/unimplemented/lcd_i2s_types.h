#ifndef DLH_LCD_I2S_TYPES_H__
#define DLH_LCD_I2S_TYPES_H__ 1

#include <stdint.h>

#ifdef __cplusplus
extern "C"
{
#endif

typedef int16_t lcd_coordinate_x;
typedef int16_t lcd_coordinate_y;

typedef uint16_t lcd_pixel_size_t;

typedef uint16_t lcd_pixel_raw_t;

typedef union lcd_pixel_u
{
	lcd_pixel_raw_t raw;
	struct colors
	{
		unsigned blue:5;
		unsigned green:6;
		unsigned red:5;
	} colors;
	uint8_t bytes[2];
} lcd_pixel_u;

typedef union lcd_pixel_meta_t
{
	struct
	{
		union
		{
			struct
			{
				uint16_t blue:5;
				uint16_t green:6;
				uint16_t red:5;
			} color;
			uint16_t color_raw;
		};
		struct
		{
			uint16_t transparent:1;
			uint16_t reserved:15;
		} flags;
	};
	uint16_t raw[2];
} lcd_pixel_meta_t;

typedef union uini32_u {
	uint8_t u8[4];
	uint16_t u16[2];
	uint32_t u32;
} uint32_u;


#ifdef __cplusplus
}
#endif

#endif
